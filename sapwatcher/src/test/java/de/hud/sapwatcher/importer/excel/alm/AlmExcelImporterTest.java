package de.hud.sapwatcher.importer.excel.alm;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import de.hud.sapwatcher.data.alm.AlmSapCompareEntry;

public class AlmExcelImporterTest {

	@Test
	public void start() throws Exception {

		AlmExcelImporter excelImporter = new AlmExcelImporter();

		Path excelFile = Paths.get(Class.class.getResource("/de/hud/sapwatcher/importer/excel/alm/Testexport_1.xls").toURI());

		List<AlmSapCompareEntry> entries = excelImporter.readFile(excelFile);

		Assert.assertNotNull(entries);
		Assert.assertEquals(8, entries.size());

		Assert.assertEquals("8000", entries.get(0).getRequirementId());
		Assert.assertEquals(8.0F, entries.get(0).getEffortPlanned(), 0);
		
		Assert.assertEquals("8005", entries.get(6).getRequirementId());
		Assert.assertEquals(24.0F, entries.get(6).getEffortPlanned(), 0);

	}
}
