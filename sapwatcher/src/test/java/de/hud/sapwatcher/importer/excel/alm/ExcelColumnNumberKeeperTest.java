package de.hud.sapwatcher.importer.excel.alm;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Assert;
import org.junit.Test;

import de.hud.sapwatcher.exception.InvalidImportFileException;

public class ExcelColumnNumberKeeperTest {

	@Test
	public void testSimple() throws Exception {
		AlmExcelColumnnumberKeeper columnnumberKeeper = new AlmExcelColumnnumberKeeper();

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("FirstSheet");
		Row myRow = sheet.createRow(5);
		int i = 1;
		myRow.createCell(i++).setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_PLANNED_EFFORT.get(0));
		myRow.createCell(i++).setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_EFFORT.get(0));
		myRow.createCell(i++).setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_REQUIREMENT_ID.get(0));
		myRow.createCell(i++).setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_REQUIREMENT_NAME.get(0));
		myRow.createCell(i++).setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_REQUIREMENT_STATUS.get(0));
		myRow.createCell(i++).setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_DEFECT_ID.get(0));
		myRow.createCell(i++).setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_DEFECT_STATUS.get(0));

		columnnumberKeeper.createColumnsBasedOnHeader(myRow);

		i = 2;
		Assert.assertEquals(i++, columnnumberKeeper.getColumnNrEffort());
	}

	@Test(expected = InvalidImportFileException.class)
	public void testMissing() throws Exception {

		AlmExcelColumnnumberKeeper columnnumberKeeper = new AlmExcelColumnnumberKeeper();

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("FirstSheet");
		Row myRow = sheet.createRow(5);
		int i = 1;
		myRow.createCell(i++).setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_EFFORT.get(0));
		// *this is missing*
		// myRow.createCell(i++).setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_REQUIREMENT_ID.get(0));

		columnnumberKeeper.createColumnsBasedOnHeader(myRow);
	}
}
