package de.hud.sapwatcher.importer.excel.sap;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Assert;
import org.junit.Test;

import de.hud.sapwatcher.exception.InvalidImportFileException;

public class ExcelColumnNumberKeeperTest {

	@Test
	public void testSimple() throws Exception {
		SapExcelColumnnumberKeeper columnnumberKeeper = new SapExcelColumnnumberKeeper();

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("FirstSheet");
		Row myRow = sheet.createRow(5);
		int i = 1;
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_CORRECTIONINDICATOR.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_STARTTIME.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_ENDTIME.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_DATE.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_PROJECT.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_TASK.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_DESCRIPTION.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_EMPLOYEE.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_SOLDSERVICE.get(0));

		columnnumberKeeper.createColumnsBasedOnHeader(myRow);

		i = 1;
		Assert.assertEquals(i++, columnnumberKeeper.getRowNumberCorrectionIndicator());
		Assert.assertEquals(i++, columnnumberKeeper.getRowNumberStartTime());
		Assert.assertEquals(i++, columnnumberKeeper.getRowNumberEndTime());
		Assert.assertEquals(i++, columnnumberKeeper.getRowNumberDate());
		Assert.assertEquals(i++, columnnumberKeeper.getRowNumberProject());
		Assert.assertEquals(i++, columnnumberKeeper.getRowNumberTask());
		Assert.assertEquals(i++, columnnumberKeeper.getRowNumberDescription());
		Assert.assertEquals(i++, columnnumberKeeper.getRowNumberEmpoyee());
		Assert.assertEquals(i++, columnnumberKeeper.getRowNumberSoldService());
	}

	@Test(expected = InvalidImportFileException.class)
	public void testMissing() throws Exception {

		SapExcelColumnnumberKeeper columnnumberKeeper = new SapExcelColumnnumberKeeper();

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("FirstSheet");
		Row myRow = sheet.createRow(5);
		int i = 1;
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_CORRECTIONINDICATOR.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_STARTTIME.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_ENDTIME.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_DATE.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_PROJECT.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_TASK.get(0));
		// missing myRow.createCell(i++).setCellValue(ExcelColumnnumberKeeper.TEXT_DESCRIPTION.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_EMPLOYEE.get(0));
		myRow.createCell(i++).setCellValue(SapExcelColumnnumberKeeper.TEXT_SOLDSERVICE.get(0));

		columnnumberKeeper.createColumnsBasedOnHeader(myRow);
	}
}
