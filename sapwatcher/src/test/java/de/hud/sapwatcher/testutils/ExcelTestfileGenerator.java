package de.hud.sapwatcher.testutils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import de.hud.sapwatcher.importer.excel.sap.SapExcelColumnnumberKeeper;

public class ExcelTestfileGenerator {

	private XSSFWorkbook wb = new XSSFWorkbook();

	private Sheet sheet;

	public ExcelTestfileGenerator() {
		sheet = wb.createSheet("Demo 1");

		// title row
		Row titleRow = sheet.createRow(0);
		createHeaderRow(titleRow);
	}

	private void setDateCell(Cell cell) {
		CellStyle style = wb.createCellStyle();
		style.setDataFormat(wb.createDataFormat().getFormat("m/d/yy"));
		cell.setCellStyle(style);
	}

	private void setTimeCell(Cell cell) {
		CellStyle style = wb.createCellStyle();
		style.setDataFormat(wb.createDataFormat().getFormat("hh:MM:ss"));
		cell.setCellStyle(style);
	}

	private Date localDateToDate(LocalDate ld) {
		Instant instant = ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		Date res = Date.from(instant);
		return res;
	}

	private Date localDateTimeToDate(LocalTime startTime) {
		Date res = new Date(startTime.toSecondOfDay() * 1000L);
		return res;
	}

	private void generateRow(Row row, String employee, String userId, String task, String project, LocalDate date, LocalTime startTime,
			LocalTime endTime, String soldService, String description) {

		int columnNr = 0;

		Cell cell = row.createCell(columnNr++);
		cell.setCellValue(employee);

		cell = row.createCell(columnNr++);
		cell.setCellValue(userId);

		cell = row.createCell(columnNr++);
		cell.setCellValue(task);

		cell = row.createCell(columnNr++);
		cell.setCellValue(project);

		cell = row.createCell(columnNr++);
		cell.setCellValue(localDateToDate(date));
		setDateCell(cell);

		cell = row.createCell(columnNr++);
		cell.setCellValue(localDateTimeToDate(startTime));
		setTimeCell(cell);

		cell = row.createCell(columnNr++);
		cell.setCellValue(localDateTimeToDate(endTime));
		setTimeCell(cell);

		cell = row.createCell(columnNr++);
		cell.setCellValue(soldService);

		cell = row.createCell(columnNr++);
		cell.setCellValue(description);
	}

	private void generateFile() {
		int rownum = 1;
		LocalDate startDate = LocalDate.of(2015, 1, 1);

		for (String employeeName : Arrays.asList("Michael Meier", "Astrid von Martinsmacher", "Axel Martin", "Mark Taschenbacher",
				"Alexandra Straußental", "Knut Brinkbäcker", "Mike Thomas", "Cora Faustschiffer", "Erik Herzogsfurt", "Mia Hartschiffer",
				"Thomas Taschenfurt", "Erik Baumhausen", "Anton Schulz-Grafenbach", "Peter Maximilian Bergfeld", "Daniela Corinna Flaschenmeier",
				"Meinhard Johannes","Lothar Körbl","Adrian Wolfgang","Traugott Bruno","Ewald Heinz","Peter Immanuel","Gunter Stephan","Ben Albert")) {
			String userId = "ID" + employeeName.toLowerCase();
			String soldService = generateRandomSoldService();

			rownum = generateEntryForUser(rownum, startDate, employeeName, userId, soldService);
		}
	}

	private int generateEntryForUser(int rownum, LocalDate startDate, String employeeName, String userId, String soldService) {
		for (int i = 1; i < 350; i++) {
			LocalDate date = startDate.plusDays(i);
			if ((date.getDayOfWeek() == DayOfWeek.SUNDAY) //
					|| (date.getDayOfWeek() == DayOfWeek.SATURDAY)) {
				continue;
			}

			// morning-entry
			rownum++;
			LocalTime startTime = LocalTime.of(8, 0);
			LocalTime endTime = startTime.plusMinutes(generateFourHourEntryWithVariance());
			String description = generateRandomDescription();
			String project = generateRandomProject();
			String task = generateRandomTask();
			generateRow(sheet.createRow(rownum), employeeName, userId, task, project, date, startTime, endTime, soldService, description);

			// afternoon-entry
			rownum++;
			startTime = endTime.plusMinutes(30);
			endTime = startTime.plusMinutes(generateFourHourEntryWithVariance());
			description = generateRandomDescription();
			project = generateRandomProject();
			task = generateRandomTask();
			generateRow(sheet.createRow(rownum), employeeName, userId, task, project, date, startTime, endTime, soldService, description);
		}
		return rownum;
	}

	private String generateRandomTask() {
		String rc = "AP ";
		Random rnd = new Random();
		rc += (rnd.nextInt(5) + 1);
		/* 1% chance to generate empty entry */
		if (rnd.nextInt(100) == 0) {
			rc = "";
		}
		return rc;
	}

	private String generateRandomSoldService() {
		String rc = "VL_Q";
		Random rnd = new Random();
		rc += (rnd.nextInt(3) + 1);
		return rc;
	}

	private String generateRandomProject() {
		String rc = "Project ";
		Random rnd = new Random();
		rc += (rnd.nextInt(5) + 1);
		return rc;
	}

	private String generateRandomDescription() {
		String rc;

		Random rnd = new Random();

		switch (rnd.nextInt(6)) {
		case 0:
			/* 1% chance to generate empty description */
			if (rnd.nextInt(100) == 0) {
				rc = "";
			} else {
				rc = "something";
				rc += String.format("%02d", rnd.nextInt(5));
			}
			break;
		case 1:
		case 2:
			rc = "DEF#4";
			rc += String.format("%03d", rnd.nextInt(30));
			break;
		default:
			rc = "REQ#8";
			rc += String.format("%03d", rnd.nextInt(10));
			break;
		}

		return rc;
	}

	private long generateFourHourEntryWithVariance() {
		Random rnd = new Random();

		return (4 * 60) + (15 * rnd.nextInt(5)) - (15 * rnd.nextInt(5));
	}

	private void writeFile(String filename) throws IOException {
		// Write the output to a file
		FileOutputStream out = new FileOutputStream(filename);
		wb.write(out);
		out.close();
	}

	private void createHeaderRow(Row titleRow) {
		int columnNr = 0;
		Cell cell = titleRow.createCell(columnNr++);
		cell.setCellValue(SapExcelColumnnumberKeeper.TEXT_EMPLOYEE.get(0));
		cell = titleRow.createCell(columnNr++);
		cell.setCellValue(SapExcelColumnnumberKeeper.TEXT_PERSONALID.get(0));
		cell = titleRow.createCell(columnNr++);
		cell.setCellValue(SapExcelColumnnumberKeeper.TEXT_TASK.get(0));
		cell = titleRow.createCell(columnNr++);
		cell.setCellValue(SapExcelColumnnumberKeeper.TEXT_PROJECT.get(0));
		cell = titleRow.createCell(columnNr++);
		cell.setCellValue(SapExcelColumnnumberKeeper.TEXT_DATE.get(0));
		cell = titleRow.createCell(columnNr++);
		cell.setCellValue(SapExcelColumnnumberKeeper.TEXT_STARTTIME.get(0));
		cell = titleRow.createCell(columnNr++);
		cell.setCellValue(SapExcelColumnnumberKeeper.TEXT_ENDTIME.get(0));
		cell = titleRow.createCell(columnNr++);
		cell.setCellValue(SapExcelColumnnumberKeeper.TEXT_SOLDSERVICE.get(0));
		cell = titleRow.createCell(columnNr++);
		cell.setCellValue(SapExcelColumnnumberKeeper.TEXT_DESCRIPTION.get(0));
	}

	public static void main(String[] args) throws IOException {
		ExcelTestfileGenerator excelTestfileGenerator = new ExcelTestfileGenerator();
		excelTestfileGenerator.generateFile();
		excelTestfileGenerator.writeFile("c:/testfile_" + System.currentTimeMillis() + ".xlsx");
		System.out.println("Finished");
	}
}
