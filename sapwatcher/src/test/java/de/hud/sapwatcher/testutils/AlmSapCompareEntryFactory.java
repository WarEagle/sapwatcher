package de.hud.sapwatcher.testutils;

import de.hud.sapwatcher.data.alm.AlmSapCompareEntry;

public class AlmSapCompareEntryFactory {

	public static AlmSapCompareEntry create(String requirementId, float effortPlanned) {
		AlmSapCompareEntry rc = new AlmSapCompareEntry();
		
		rc.setEffort(effortPlanned);
		rc.setRequirementId(requirementId);
		
		return rc;
	}

	public static AlmSapCompareEntry createWithDefect(String requirementId, float effortPlanned, String defectId) {
		AlmSapCompareEntry rc = new AlmSapCompareEntry();
		
		rc.setEffort(effortPlanned);
		rc.setRequirementId(requirementId);
		rc.setDefectId(defectId);
		
		return rc;
	}
}
