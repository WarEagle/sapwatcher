package de.hud.sapwatcher.testutils;

import java.time.LocalDateTime;

import de.hud.sapwatcher.data.WorkingTimeEntry;

public class WorkingTimeEntryFactory {

	public static WorkingTimeEntry create(String name, int duration) {
		WorkingTimeEntry rc = new WorkingTimeEntry(name, LocalDateTime.now(), LocalDateTime.now().plusHours(duration), "Project", "Task",
				"Description", "Q1");
		return rc;
	}

	public static WorkingTimeEntry create(String name, String requirementName, int duration) {
		WorkingTimeEntry rc = new WorkingTimeEntry(name, LocalDateTime.now(), LocalDateTime.now().plusHours(duration), "Project", "Task",
				requirementName, "Q1");
		return rc;
	}

	public static WorkingTimeEntry create(String name, LocalDateTime start, int duration) {
		WorkingTimeEntry rc = new WorkingTimeEntry(name, start, start.plusHours(duration), "Project", "Task", "Description", "Q1");
		return rc;
	}
}
