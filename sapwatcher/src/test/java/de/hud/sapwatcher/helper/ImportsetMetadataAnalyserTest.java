package de.hud.sapwatcher.helper;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.data.ImportsetMetadata;
import de.hud.sapwatcher.data.WorkingTimeEntry;
import de.hud.sapwatcher.importer.excel.sap.SapExcelImporter;

public class ImportsetMetadataAnalyserTest {

	@Test
	public void testImportsetMetadataAnalyserPL1() throws Exception {

		SapExcelImporter excelImporter = new SapExcelImporter();

		Path excelFile = Paths.get(Class.class.getResource("/de/hud/sapwatcher/Testset_PL1.XLSX").toURI());

		List<WorkingTimeEntry> workingTimeEntries = excelImporter.readFile(excelFile);
		DataStorage dataStorage = new DataStorage();
		dataStorage.storeWorkOrderEntries(workingTimeEntries);

		ImportsetMetadata importsetMetadata = ImportsetMetadataAnalyser.analyseData(dataStorage);

		Assert.assertEquals(6, importsetMetadata.getNumberProjects());
		Assert.assertEquals(2, importsetMetadata.getNumberEmployee());
		Assert.assertEquals(90, importsetMetadata.getNumberEntries());
		Assert.assertEquals(272.5, importsetMetadata.getCountHours(), 0);
	}

	@Test
	public void testImportsetMetadataAnalyserUser1() throws Exception {

		SapExcelImporter excelImporter = new SapExcelImporter();

		Path excelFile = Paths.get(Class.class.getResource("/de/hud/sapwatcher/Testset_User1.XLSX").toURI());

		List<WorkingTimeEntry> workingTimeEntries = excelImporter.readFile(excelFile);
		DataStorage dataStorage = new DataStorage();
		dataStorage.storeWorkOrderEntries(workingTimeEntries);

		ImportsetMetadata importsetMetadata = ImportsetMetadataAnalyser.analyseData(dataStorage);

		Assert.assertEquals(10, importsetMetadata.getNumberProjects());
		Assert.assertEquals(1, importsetMetadata.getNumberEmployee());
		Assert.assertEquals(497, importsetMetadata.getNumberEntries());
		Assert.assertEquals(1820.5, importsetMetadata.getCountHours(), 0);
	}
	
	@Test
	public void testImportsetMetadataAnalyserUser4() throws Exception {
		SapExcelImporter excelImporter = new SapExcelImporter();

		Path excelFile = Paths.get(Class.class.getResource("/de/hud/sapwatcher/Testset_User4.XLSX").toURI());

		List<WorkingTimeEntry> workingTimeEntries = excelImporter.readFile(excelFile);
		DataStorage dataStorage = new DataStorage();
		dataStorage.storeWorkOrderEntries(workingTimeEntries);

		ImportsetMetadata importsetMetadata = ImportsetMetadataAnalyser.analyseData(dataStorage);

		Assert.assertEquals(5, importsetMetadata.getNumberProjects());
		Assert.assertEquals(8, importsetMetadata.getNumberEmployee());
		Assert.assertEquals(1136, importsetMetadata.getNumberEntries());
		Assert.assertEquals(4557.75, importsetMetadata.getCountHours(), 0);
	}

}
