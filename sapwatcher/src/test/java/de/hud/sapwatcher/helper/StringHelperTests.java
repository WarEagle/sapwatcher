package de.hud.sapwatcher.helper;

import java.time.LocalDateTime;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

public class StringHelperTests {
	@Test
	public void testShorter() {
		String result;
		
		result = StringHelper.shortenName("Hans Waldemar");
		Assert.assertEquals("Ha.Wa.", result);

		result = StringHelper.shortenName("srdht tdzhjruh grsgth zjsuht grs<ytzju h");
		Assert.assertEquals("sr.td.gr.zj.gr.h.", result);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testCreateTimeFromPartialStrings() {

		Date date = new Date(2014 - 1900, 12 - 1, 11, 10, 9, 8);
		Date time = new Date(2013 - 1900, 5 - 1, 6, 8, 30, 0);
		LocalDateTime resultTime = StringHelper.createTimeFromPartialStrings(date, time);

		Assert.assertEquals(11, resultTime.getDayOfMonth());
		Assert.assertEquals(12, resultTime.getMonthValue());
		Assert.assertEquals(2014, resultTime.getYear());

		Assert.assertEquals(8, resultTime.getHour());
		Assert.assertEquals(30, resultTime.getMinute());
		Assert.assertEquals(0, resultTime.getSecond());
	}
}
