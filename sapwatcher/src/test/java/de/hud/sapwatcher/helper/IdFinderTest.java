package de.hud.sapwatcher.helper;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class IdFinderTest {

	@Test
	public void noMatch() {

		String text;
		List<String> result;

		// test
		text = "NothingHere";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(0, result.size());

		// test
		text = "Nothing But Numbers Small 11 And High 47 Here";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(0, result.size());

		// test
		text = "Years have to be ignored, so something like 2014 is wrong, but 4711 is correct.";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		assertThat(result, hasItems("Requirement 4711"));

	}

	@Test
	public void testSimpleDefect() {

		String text;
		List<String> result;

		// test
		text = "DEF#4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

		// test
		text = "DEFECT#4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

		// test
		text = "DEF#14711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 14711", result.get(0));

		// test
		text = "DEFECT#14711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 14711", result.get(0));

		// test
		text = "DEFect#4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

		// test
		text = "def#4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

		// test
		text = "DEF4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

		// test
		text = "defect4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

		// test
		text = "defect 4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

		// test
		text = "def 4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

		// test
		text = "D4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

		// test
		text = "d4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

	}

	@Test
	public void testSimpleDefectWithWhitespace() {
		new IdFinder();

		String text;
		List<String> result;

		// test 1
		text = "def#4711";

		result = IdFinder.findIds("   " + text + "   ");
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

		// test 2
		text = "def#4711";

		result = IdFinder.findIds("  \t " + text + " \n  ");
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

	}

	@Test
	public void testSimpleRequirement() {
		new IdFinder();

		String text;
		List<String> result;

		// test
		text = "REQ#4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Requirement 4711", result.get(0));

		// test
		text = "REQ#4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Requirement 4711", result.get(0));

		// test
		text = "R#4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Requirement 4711", result.get(0));

		// test
		text = "r#4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Requirement 4711", result.get(0));

		// test
		text = "R 4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Requirement 4711", result.get(0));

		// test
		text = "r 4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Requirement 4711", result.get(0));

		// test
		text = "R4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Requirement 4711", result.get(0));

		// test
		text = "r4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Requirement 4711", result.get(0));

		// test
		text = "REQUIREMENT 4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Requirement 4711", result.get(0));

		// test
		text = "Requirement 4711";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Requirement 4711", result.get(0));

	}

	@Test
	public void testSimpleRequirementWithWhitespace() {
		new IdFinder();

		String text;
		List<String> result;

		// test 1
		text = "req#4711";

		result = IdFinder.findIds("   " + text + "   ");
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Requirement 4711", result.get(0));

		// test 2
		text = "req#4711";

		result = IdFinder.findIds("  \t " + text + " \n  ");
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Requirement 4711", result.get(0));

	}

	@Test
	public void testLittleComplexDefect() {
		new IdFinder();

		String text;
		List<String> result;

		// test 1
		text = "Some DEF#4711 here";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

		// test 2
		text = "Some def#4711 here";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("Defect 4711", result.get(0));

		// test 3
		text = "Two def#4711 here and def#4712 here";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(2, result.size());
		assertThat(result, hasItems("Defect 4712", "Defect 4711"));
		

		// test 4
		text = "Two DEF#4711 here and DEF#4712 here";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(2, result.size());
		assertThat(result, hasItems("Defect 4712", "Defect 4711"));

	}

	@Test
	public void testMixedEntries() {
		new IdFinder();

		String text;
		List<String> result;

		// test
		text = "Some DEF#4711 here and a REQ#4742 also";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(2, result.size());
		assertThat(result, hasItems("Requirement 4742", "Defect 4711"));

		// test
		text = "Some Requirement 4711 here and a Def 4742 also";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(2, result.size());
		assertThat(result, hasItems("Defect 4742", "Requirement 4711"));

	}
	
	@Test
	public void testOnlyNumbers() {
		new IdFinder();

		String text;
		List<String> result;

		// test
		text = "4711 and this one 9872";

		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		assertThat(result, hasItems("Defect 9872", "Requirement 4711"));

		// test
		text = "Some Defect 4711, 4742 also";
		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		assertThat(result, hasItems("Requirement 4742", "Defect 4711"));
		
		text = "Req. 4599, 4600 - refactoring";
		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		assertThat(result, hasItems("Requirement 4599", "Requirement 4600"));

		text = "#6701, #8029 ";
		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		Assert.assertEquals(2, result.size());
		assertThat(result, hasItems("Defect 6701", "Defect 8029"));

		text = "#8020, #8021 angelegt, #6701 behoben, De";
		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		assertThat(result, hasItems("Defect 8020", "Defect 8020", "Defect 6701"));

		text = "#8024 SQL erstellt, PROD 4.2 TWN Role";
		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		assertThat(result, hasItems("Defect 8024"));

		text = "4648/4653 Calculation ETA";
		result = IdFinder.findIds(text);
		Assert.assertNotNull(result);
		assertThat(result, hasItems("Requirement 4648", "Requirement 4648"));


	}
	
}
