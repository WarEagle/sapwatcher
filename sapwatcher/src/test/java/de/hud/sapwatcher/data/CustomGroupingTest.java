package de.hud.sapwatcher.data;

import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Test;

public class CustomGroupingTest {
	final String USER1 = "User1";

	final String PROJECT1 = "Project1";

	final String TASK1 = "Task1";

	final String DESCRIPTION1 = "Description1";

	final String SERVICE1 = "Service1";

	@Test
	public void emptyGrouping() {
		WorkingTimeEntry workingTimeEntry = new WorkingTimeEntry(USER1, LocalDateTime.now().minusHours(2), LocalDateTime.now(), PROJECT1, TASK1,
				DESCRIPTION1, SERVICE1);

		CustomGrouping customGrouping = new CustomGrouping("MyGrouping", true, "", "", "", "");

		boolean equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);

		Assert.assertEquals(Boolean.TRUE, equals);
	}

	@Test
	public void simpleMisMatch() {
		WorkingTimeEntry workingTimeEntry = new WorkingTimeEntry(USER1, LocalDateTime.now().minusHours(2), LocalDateTime.now(), PROJECT1, TASK1,
				DESCRIPTION1, SERVICE1);

		CustomGrouping customGrouping = new CustomGrouping("MyGrouping", true, PROJECT1, "Something", "", "");
		boolean equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);
		Assert.assertEquals(Boolean.FALSE, equals);
	}

	@Test
	public void simpleMatch() {
		WorkingTimeEntry workingTimeEntry = new WorkingTimeEntry(USER1, LocalDateTime.now().minusHours(2), LocalDateTime.now(), PROJECT1, TASK1,
				DESCRIPTION1, SERVICE1);

		CustomGrouping customGrouping = new CustomGrouping("MyGrouping", true, PROJECT1, "", "", "");
		boolean equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);
		Assert.assertEquals(Boolean.TRUE, equals);

		customGrouping = new CustomGrouping("MyGrouping", true, PROJECT1, DESCRIPTION1, "", "");
		equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);
		Assert.assertEquals(Boolean.TRUE, equals);

		customGrouping = new CustomGrouping("MyGrouping", true, PROJECT1, DESCRIPTION1, TASK1, "");
		equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);
		Assert.assertEquals(Boolean.TRUE, equals);
	}

	@Test
	public void matchWithWildcards() {
		WorkingTimeEntry workingTimeEntry = new WorkingTimeEntry(USER1, LocalDateTime.now().minusHours(2), LocalDateTime.now(), PROJECT1, TASK1,
				DESCRIPTION1, SERVICE1);

		CustomGrouping customGrouping = new CustomGrouping("MyGrouping", true, ".*p.*", "", "", "");
		boolean equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);
		Assert.assertEquals(Boolean.TRUE, equals);

		customGrouping = new CustomGrouping("MyGrouping", true, ".*P.*", "", "", "");
		equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);
		Assert.assertEquals(Boolean.TRUE, equals);

		customGrouping = new CustomGrouping("MyGrouping", true, ".*X.*", "", "", "");
		equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);
		Assert.assertEquals(Boolean.FALSE, equals);

		customGrouping = new CustomGrouping("MyGrouping", true, ".*P.*", ".*D.*", "", "");
		equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);
		Assert.assertEquals(Boolean.TRUE, equals);

		customGrouping = new CustomGrouping("MyGrouping", true, ".*P.*", ".*X.*", "", "");
		equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);
		Assert.assertEquals(Boolean.FALSE, equals);

		customGrouping = new CustomGrouping("MyGrouping", true, ".*P.*", ".*D.*", ".*T.*", "");
		equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);
		Assert.assertEquals(Boolean.TRUE, equals);

		customGrouping = new CustomGrouping("MyGrouping", true, ".*P.*", ".*D.*", ".*X.*", "");
		equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);
		Assert.assertEquals(Boolean.FALSE, equals);

		customGrouping = new CustomGrouping("MyGrouping", true, ".*P.*", ".*D.*", ".*T.*", ".*U.*");
		equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);
		Assert.assertEquals(Boolean.TRUE, equals);

		customGrouping = new CustomGrouping("MyGrouping", true, ".*P.*", ".*D.*", ".*T.*", ".*X.*");
		equals = customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry);
		Assert.assertEquals(Boolean.FALSE, equals);
	}

}
