package de.hud.sapwatcher.data;

import java.time.LocalDateTime;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class WorkingTimeEntryTest {

	@Test
	public void testWorkingTimeEntryOnlyDescription() throws Exception {

		WorkingTimeEntry entry = new WorkingTimeEntry("Hans", LocalDateTime.now().minusHours(2), LocalDateTime.now(),
				"Project1", "Task1", "Description1", "Q1");
		Map<String, Float> splittedDescription = entry.getSplittedDescription();

		Assert.assertNotNull(splittedDescription.size());
		Assert.assertEquals(1, splittedDescription.size());
		Assert.assertEquals("Description1", splittedDescription.keySet().toArray()[0]);
		Assert.assertEquals(2, splittedDescription.get("Description1"), 0);

	}

	@Test
	public void testWorkingTimeOneDefect() throws Exception {

		String description = "DEF#4711";
		
		WorkingTimeEntry entry = new WorkingTimeEntry("Hans", LocalDateTime.now().minusHours(2), LocalDateTime.now(),
				"Project1", "Task1", description, "Q1");
		Map<String, Float> splittedDescription = entry.getSplittedDescription();

		Assert.assertNotNull(splittedDescription.size());
		Assert.assertEquals(1, splittedDescription.size());
		Assert.assertEquals("Defect 4711", splittedDescription.keySet().toArray()[0]);
		Assert.assertEquals(2, splittedDescription.get("Defect 4711"), 0);

	}

	@Test
	public void testWorkingTimeTwoDefect() throws Exception {

		String description = "DEF#4711, DEF#1508";
		
		WorkingTimeEntry entry = new WorkingTimeEntry("Hans", LocalDateTime.now().minusHours(2), LocalDateTime.now(),
				"Project1", "Task1", description, "Q1");
		Map<String, Float> splittedDescription = entry.getSplittedDescription();

		Assert.assertNotNull(splittedDescription.size());
		Assert.assertEquals(2, splittedDescription.size());
		Assert.assertEquals(1, splittedDescription.get("Defect 4711"), 0);
		Assert.assertEquals(1, splittedDescription.get("Defect 1508"), 0);

	}

}
