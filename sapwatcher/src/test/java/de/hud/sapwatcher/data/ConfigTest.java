package de.hud.sapwatcher.data;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ConfigTest {

	String filename = "sapwatcher_junit.xml";

	@After
	@Before
	public void before() {
		File file = new File(filename);
		if (file.exists()) {
			boolean delete = file.delete();
			if (!delete) {
				Assert.fail("Coult not delete " + filename);
			}
		}
	}

	@Test
	public void test() {
		boolean exceptionThrown = false;
		Config config = new Config(filename);

		try {
			ArrayList<CustomGrouping> customGroupings = config.getCustomGroupings();
			customGroupings.add(new CustomGrouping("Neu", true, null, null, null, null));

			Assert.assertEquals(1, config.getCustomGroupings().size());

			config.readFromFile();
		} catch (JAXBException e) {
			e.printStackTrace();
			exceptionThrown = true;
		}
		Assert.assertFalse(exceptionThrown);
		Assert.assertEquals(0, config.getCustomGroupings().size());
	}

	@Test
	public void testWriteRead() {
		boolean exceptionThrown = false;
		Config config = new Config(filename);

		try {
			Assert.assertEquals(0, config.getCustomGroupings().size());
			ArrayList<CustomGrouping> customGroupings = config.getCustomGroupings();
			customGroupings.add(new CustomGrouping("Dummy", true, null, null, null, null));
			Assert.assertEquals(1, config.getCustomGroupings().size());
			Assert.assertEquals("Dummy", config.getCustomGroupings().get(0).getName());

			config.writeToFile();
			config = new Config(filename);
			Assert.assertEquals(0, config.getCustomGroupings().size());

			config.readFromFile();

			Assert.assertEquals(1, config.getCustomGroupings().size());
			Assert.assertEquals("Dummy", config.getCustomGroupings().get(0).getName());
		} catch (JAXBException | IOException e) {
			e.printStackTrace();
			exceptionThrown = true;
		}
		Assert.assertFalse(exceptionThrown);

	}
}
