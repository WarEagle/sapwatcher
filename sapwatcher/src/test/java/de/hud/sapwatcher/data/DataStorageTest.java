package de.hud.sapwatcher.data;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import de.hud.sapwatcher.importer.excel.sap.SapExcelImporter;

public class DataStorageTest {

	@Test
	public void testImportsetMetadataAnalyserPL1() throws Exception {

		SapExcelImporter excelImporter = new SapExcelImporter();

		Path excelFile = Paths.get(Class.class.getResource("/de/hud/sapwatcher/Testset_PL1.XLSX").toURI());

		List<WorkingTimeEntry> workingTimeEntries = excelImporter.readFile(excelFile);
		DataStorage dataStorage = new DataStorage();
		dataStorage.storeWorkOrderEntries(workingTimeEntries);

		List<String> listNamesProjects = dataStorage.getListNamesProjects();
		List<String> listNamesEmployee = dataStorage.getListNamesEmployee();

		Assert.assertEquals(6, listNamesProjects.size());
		Assert.assertEquals(2, listNamesEmployee.size());
		Assert.assertEquals(90, dataStorage.getAllWorkingTimeEntries().size());

		List<WorkingTimeEntry> workingTimeEntriesForEmployee = dataStorage.getWorkingTimeEntriesForEmployee("Testuser Hans");
		Assert.assertEquals(55, workingTimeEntriesForEmployee.size());

		workingTimeEntriesForEmployee = dataStorage.getWorkingTimeEntriesForEmployee("Marsianer Drei");
		Assert.assertEquals(35, workingTimeEntriesForEmployee.size());

		List<WorkingTimeEntry> workingTimeEntriesForProject = dataStorage.getWorkingTimeEntriesForProject("H-304001300.01.01.12.02");
		Assert.assertEquals(43, workingTimeEntriesForProject.size());

		workingTimeEntriesForProject = dataStorage.getWorkingTimeEntriesForProject("E-3000.06.01");
		Assert.assertEquals(2, workingTimeEntriesForProject.size());

	}

	@Test
	public void testGroupByDay() throws Exception {
		SapExcelImporter excelImporter = new SapExcelImporter();

		Path excelFile = Paths.get(Class.class.getResource("/de/hud/sapwatcher/Testset_PL1.XLSX").toURI());

		List<WorkingTimeEntry> workingTimeEntries = excelImporter.readFile(excelFile);
		DataStorage dataStorage = new DataStorage();
		dataStorage.storeWorkOrderEntries(workingTimeEntries);
		
		Map<LocalDate, List<WorkingTimeEntry>> workingTimeEntriesGroupedByDay = dataStorage.getWorkingTimeEntriesGroupedByDay();
		Assert.assertNotNull(workingTimeEntriesGroupedByDay);
		Assert.assertNotNull(workingTimeEntriesGroupedByDay.keySet());
		Assert.assertEquals(20, workingTimeEntriesGroupedByDay.keySet().size());

	}

}
