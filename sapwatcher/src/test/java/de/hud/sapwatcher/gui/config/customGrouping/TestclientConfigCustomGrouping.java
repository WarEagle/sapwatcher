package de.hud.sapwatcher.gui.config.customGrouping;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import de.hud.sapwatcher.gui.mainmenu.MainMenuController;

public class TestclientConfigCustomGrouping extends Application {

	public TestclientConfigCustomGrouping() {
	}

	public static void main(String[] args) {
		launch(args);
	}

	@SuppressWarnings("unused")
	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader view = new FXMLLoader(
				MainMenuController.class
						.getResource("/de/hud/sapwatcher/gui/config/customGrouping/CustomGroupingView.fxml"));

		Pane pane = view.load();
		CustomGroupingController controller = view.getController();

		Scene scene = new Scene(pane);

		primaryStage.setScene(scene);
		primaryStage.show();

	}
}
