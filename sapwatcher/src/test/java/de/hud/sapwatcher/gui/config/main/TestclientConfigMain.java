package de.hud.sapwatcher.gui.config.main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import de.hud.sapwatcher.data.Config;
import de.hud.sapwatcher.gui.mainmenu.MainMenuController;

public class TestclientConfigMain extends Application {

	public TestclientConfigMain() {
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TestclientConfigCustomGrouping testclient = new TestclientConfigCustomGrouping();

		FXMLLoader view = new FXMLLoader(
				MainMenuController.class
						.getResource("/de/hud/sapwatcher/gui/config/main/ConfigMainView.fxml"));

		Pane pane = view.load();
		ConfigMainController controller = view.getController();
		Config config = new Config();
		config.readFromFile();
		
		controller.setConfig(config);

		Scene scene = new Scene(pane);

		primaryStage.setScene(scene);
		primaryStage.show();

	}
}
