package de.hud.sapwatcher.gui.mainmenu.analysis;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import de.hud.sapwatcher.data.Config;
import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.data.WorkingTimeEntry;
import de.hud.sapwatcher.gui.mainmenu.MainMenuController;
import de.hud.sapwatcher.importer.excel.sap.SapExcelImporter;

public class TestclientMainAnalysis extends Application {

	public TestclientMainAnalysis() {
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TestclientConfigCustomGrouping testclient = new TestclientConfigCustomGrouping();

		FXMLLoader view = new FXMLLoader(
				MainMenuController.class
						.getResource("/de/hud/sapwatcher/gui/mainmenu/analysis/AnalysisView.fxml"));

		Pane pane = view.load();
		AnalysisController controller = view.getController();
		Config config = new Config();
		config.readFromFile();
		
		SapExcelImporter excelImporter = new SapExcelImporter();

		Path excelFile = Paths.get(Class.class.getResource("/de/hud/sapwatcher/Testset_PL1.XLSX").toURI());

		List<WorkingTimeEntry> workingTimeEntries = excelImporter.readFile(excelFile);
		DataStorage dataStorage = new DataStorage();
		dataStorage.storeWorkOrderEntries(workingTimeEntries);

		controller.refreshAnalysis(dataStorage);
		
		Scene scene = new Scene(pane);

		primaryStage.setScene(scene);
		primaryStage.show();

	}
}
