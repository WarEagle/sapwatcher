package de.hud.sapwatcher.exporter.gui.workingtime;

import java.time.DayOfWeek;
import java.util.HashMap;
import java.util.Map;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class TestclientWorkingTime extends Application {

	public TestclientWorkingTime() {
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		BorderPane pane = new BorderPane();
		
		Map<DayOfWeek, Float> data = new HashMap<>();
		
		data.put(DayOfWeek.MONDAY, 10.5f);
		data.put(DayOfWeek.TUESDAY, 8f);
		data.put(DayOfWeek.WEDNESDAY, 1.5f);
		data.put(DayOfWeek.THURSDAY, 5.5f);
		data.put(DayOfWeek.FRIDAY, 8.5f);
		
		DayOfMonthPaneGenerator dayOfWeekPane = new DayOfMonthPaneGenerator(data , "Test");
		
		pane.setCenter(dayOfWeekPane.generatePane());
		
		Scene scene = new Scene(pane, 500, 500);

		primaryStage.setScene(scene);
		primaryStage.show();

	}
}
