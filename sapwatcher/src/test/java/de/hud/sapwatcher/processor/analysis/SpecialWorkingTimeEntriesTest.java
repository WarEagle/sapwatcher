package de.hud.sapwatcher.processor.analysis;

import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Test;

import de.hud.sapwatcher.data.WorkingTimeEntry;
import de.hud.sapwatcher.testutils.WorkingTimeEntryFactory;

public class SpecialWorkingTimeEntriesTest {

	@Test
	public void testIsEmptyTask() {
		WorkingTimeEntry workingTimeEntry;
		SpecialWorkingTimeEntries specialWorkingTimeEntries = new SpecialWorkingTimeEntries(null);

		workingTimeEntry = WorkingTimeEntryFactory.create("SomeTitle", 1);
		Assert.assertFalse(specialWorkingTimeEntries.isEmptyTask(workingTimeEntry));

		workingTimeEntry = new WorkingTimeEntry("Employee", LocalDateTime.now(), LocalDateTime.now(), "Project", null, "Description", "SoldService");
		Assert.assertTrue(specialWorkingTimeEntries.isEmptyTask(workingTimeEntry));

		workingTimeEntry = new WorkingTimeEntry("Employee", LocalDateTime.now(), LocalDateTime.now(), "Project", "", "Description", "SoldService");
		Assert.assertTrue(specialWorkingTimeEntries.isEmptyTask(workingTimeEntry));

		workingTimeEntry = new WorkingTimeEntry("Employee", LocalDateTime.now(), LocalDateTime.now(), "Project", " ", "Description", "SoldService");
		Assert.assertTrue(specialWorkingTimeEntries.isEmptyTask(workingTimeEntry));

		workingTimeEntry = new WorkingTimeEntry("Employee", LocalDateTime.now(), LocalDateTime.now(), "Project", "<LEER>", "Description",
				"SoldService");
		Assert.assertTrue(specialWorkingTimeEntries.isEmptyTask(workingTimeEntry));

		workingTimeEntry = new WorkingTimeEntry("Employee", LocalDateTime.now(), LocalDateTime.now(), "Project", "<leer>", "Description",
				"SoldService");
		Assert.assertTrue(specialWorkingTimeEntries.isEmptyTask(workingTimeEntry));

	}

	@Test
	public void testIsEmptyDescription() {
		WorkingTimeEntry workingTimeEntry;
		SpecialWorkingTimeEntries specialWorkingTimeEntries = new SpecialWorkingTimeEntries(null);

		workingTimeEntry = WorkingTimeEntryFactory.create("SomeTitle", 1);
		Assert.assertFalse(specialWorkingTimeEntries.isEmptyDescription(workingTimeEntry));

		workingTimeEntry =new WorkingTimeEntry("Employee", LocalDateTime.now(), LocalDateTime.now(), "Project", "Task", null,  "SoldService");
		Assert.assertTrue(specialWorkingTimeEntries.isEmptyDescription(workingTimeEntry));

		workingTimeEntry =new WorkingTimeEntry("Employee", LocalDateTime.now(), LocalDateTime.now(), "Project", "Task", "",  "SoldService");
		Assert.assertTrue(specialWorkingTimeEntries.isEmptyDescription(workingTimeEntry));

		workingTimeEntry =new WorkingTimeEntry("Employee", LocalDateTime.now(), LocalDateTime.now(), "Project", "Task", " ",  "SoldService");
		Assert.assertTrue(specialWorkingTimeEntries.isEmptyDescription(workingTimeEntry));

		workingTimeEntry =new WorkingTimeEntry("Employee", LocalDateTime.now(), LocalDateTime.now(), "Project", "Task", "<LEER>",  "SoldService");
		Assert.assertTrue(specialWorkingTimeEntries.isEmptyDescription(workingTimeEntry));

		workingTimeEntry =new WorkingTimeEntry("Employee", LocalDateTime.now(), LocalDateTime.now(), "Project", "Task", "<leer>",  "SoldService");
		Assert.assertTrue(specialWorkingTimeEntries.isEmptyDescription(workingTimeEntry));

		workingTimeEntry =new WorkingTimeEntry("Employee", LocalDateTime.now(), LocalDateTime.now(), "Project", "Task", ".",  "SoldService");
		Assert.assertTrue(specialWorkingTimeEntries.isEmptyDescription(workingTimeEntry));

		workingTimeEntry =new WorkingTimeEntry("Employee", LocalDateTime.now(), LocalDateTime.now(), "Project", "Task", "..",  "SoldService");
		Assert.assertTrue(specialWorkingTimeEntries.isEmptyDescription(workingTimeEntry));

		workingTimeEntry =new WorkingTimeEntry("Employee", LocalDateTime.now(), LocalDateTime.now(), "Project", "Task", "...", "SoldService");
		Assert.assertTrue(specialWorkingTimeEntries.isEmptyDescription(workingTimeEntry));
	}
}
