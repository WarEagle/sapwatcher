package de.hud.sapwatcher.processor.analysis;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.data.WorkingTimeEntry;
import de.hud.sapwatcher.testutils.WorkingTimeEntryFactory;

public class AverageWorkingtimeTest {

	@Test
	public void simpleTest() {
		DataStorage dataStorage = new DataStorage();
		List<WorkingTimeEntry> workingTimeEntries = new ArrayList<>();
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA1", 1));
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA2", 2));
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA3", 3));

		dataStorage.storeWorkOrderEntries(workingTimeEntries);
		AverageWorkingtime averageWorkingtime = new AverageWorkingtime(dataStorage);

		Map<DayOfWeek, Float> timePerWeekday = averageWorkingtime.getTimePerWeekday();

		Assert.assertNotNull(timePerWeekday);
		Assert.assertEquals(1, timePerWeekday.size());
		Assert.assertEquals(workingTimeEntries.get(0).getStartDate().getDayOfWeek(), timePerWeekday.keySet().iterator().next());
		DayOfWeek dayOfWeek = workingTimeEntries.get(0).getStartDate().getDayOfWeek();
		Assert.assertEquals(2, timePerWeekday.get(dayOfWeek).floatValue(),0);

	}

	@Test
	public void simpleTest2() {
		DataStorage dataStorage = new DataStorage();
		List<WorkingTimeEntry> workingTimeEntries = new ArrayList<>();
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA1", 1));
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA2", 2));
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA3", LocalDateTime.now().minusDays(2), 3));

		dataStorage.storeWorkOrderEntries(workingTimeEntries);
		AverageWorkingtime averageWorkingtime = new AverageWorkingtime(dataStorage);

		Map<DayOfWeek, Float> timePerWeekday = averageWorkingtime.getTimePerWeekday();
		//DayOfWeek dayOfWeek = workingTimeEntries.get(0).getStartDate().getDayOfWeek();
//		Assert.assertEquals(2, timePerWeekday.get(dayOfWeek).floatValue(),0);

		Assert.assertNotNull(timePerWeekday);
		Assert.assertEquals(2, timePerWeekday.size());

		// TODO
	}

}
