package de.hud.sapwatcher.processor.alm;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.data.OutputDataEntry;
import de.hud.sapwatcher.data.OutputDataStructure;
import de.hud.sapwatcher.data.WorkingTimeEntry;
import de.hud.sapwatcher.data.alm.AlmSapCompareEntry;
import de.hud.sapwatcher.data.alm.AlmSapCompareOutputEntry;
import de.hud.sapwatcher.testutils.AlmSapCompareEntryFactory;
import de.hud.sapwatcher.testutils.WorkingTimeEntryFactory;

public class AlmCompareGeneratorTest {

	@Test
	public void testAlmCompareGeneratorNoAlmData() {
		DataStorage dataStorage = new DataStorage();
		AlmCompareGenerator generator = new AlmCompareGenerator();

		List<WorkingTimeEntry> workingTimeEntries = new ArrayList<>();
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA1", "4711", 1));
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA2", "4711", 2));
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA3", "4712", 3));

		dataStorage.storeWorkOrderEntries(workingTimeEntries);

		generator.setDataStorageSap(dataStorage);
		List<AlmSapCompareOutputEntry> entries = generator.compareEntries(false);
		Assert.assertNotNull(entries);
		Assert.assertEquals(2, entries.size());

	}

	@Test
	public void testAlmCompareGeneratorMatchingAlmData() {
		DataStorage dataStorage = new DataStorage();
		AlmCompareGenerator generator = new AlmCompareGenerator();

		List<WorkingTimeEntry> workingTimeEntries = new ArrayList<>();
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA1", "4711", 1));
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA2", "4711", 2));
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA3", "4712", 3));

		List<AlmSapCompareEntry> entriesAlm = new ArrayList<>();
		entriesAlm.add(AlmSapCompareEntryFactory.create("4710", 7));
		entriesAlm.add(AlmSapCompareEntryFactory.create("4711", 5));
		entriesAlm.add(AlmSapCompareEntryFactory.create("4712", 3));

		dataStorage.storeWorkOrderEntries(workingTimeEntries);
		generator.setDataStorageSap(dataStorage);
		generator.setAlmData(entriesAlm);
		List<AlmSapCompareOutputEntry> entries = generator.compareEntries(false);
		Assert.assertNotNull(entries);
		Assert.assertEquals(3, entries.size());
	}

	@Test
	public void testAlmCompareGeneratorMoreInSap() {
		DataStorage dataStorage = new DataStorage();
		AlmCompareGenerator generator = new AlmCompareGenerator();

		List<WorkingTimeEntry> workingTimeEntries = new ArrayList<>();
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA1", "4711", 1));
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA2", "4711", 2));
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA3", "4712", 3));
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA3", "4713", 4));
		workingTimeEntries.add(WorkingTimeEntryFactory.create("MA3", "4714", 5));

		List<AlmSapCompareEntry> entriesAlm = new ArrayList<>();
		entriesAlm.add(AlmSapCompareEntryFactory.create("4710", 7));
		entriesAlm.add(AlmSapCompareEntryFactory.create("4711", 5));
		entriesAlm.add(AlmSapCompareEntryFactory.create("4712", 3));

		dataStorage.storeWorkOrderEntries(workingTimeEntries);
		generator.setDataStorageSap(dataStorage);
		generator.setAlmData(entriesAlm);
		List<AlmSapCompareOutputEntry> entries = generator.compareEntries(false);
		Assert.assertNotNull(entries);
		Assert.assertEquals(5, entries.size());
	}

	@Test
	@Ignore
	public void addSapEntryIfExistsInSap() {
		AlmCompareGenerator generator = new AlmCompareGenerator();
		OutputDataStructure outputDataStructure = new OutputDataStructure("Alles");
		OutputDataEntry subEntry = new OutputDataEntry("SomeName");
		subEntry.setHours(10.0f);
		outputDataStructure.addSubEntry("REQ#4711", subEntry);

		subEntry = new OutputDataEntry("DEF#1234");
		subEntry.setHours(5f);
		outputDataStructure.addSubEntry("DEF#1234", subEntry);

		subEntry = new OutputDataEntry("DEF#1234");
		subEntry.setHours(5f);
		outputDataStructure.addEntry("DEF#1234", 5.0f);
		outputDataStructure.addSubEntry("DEF#1234", subEntry);

		subEntry = new OutputDataEntry("DEF#5678");
		subEntry.setHours(2f);
		outputDataStructure.addSubEntry("DEF#5678", subEntry);
		AlmSapCompareEntry entryAlm = AlmSapCompareEntryFactory.createWithDefect("4711", 10, "1234");

		List<AlmSapCompareOutputEntry> rc = new ArrayList<>();
		generator.addSapEntryIfExistsInSap(entryAlm, rc, outputDataStructure, true);

		Assert.assertNotNull(rc);
		Assert.assertEquals(2, rc.size());
		Assert.assertEquals(15, rc.get(0).getEffortCurrentTotal() + rc.get(1).getEffortCurrentTotal(), 0);

	}
}
