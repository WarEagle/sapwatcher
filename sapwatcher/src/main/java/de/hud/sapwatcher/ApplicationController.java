package de.hud.sapwatcher;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.Config;
import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.data.ImportsetMetadata;
import de.hud.sapwatcher.data.WorkingTimeEntry;
import de.hud.sapwatcher.exception.InvalidImportFileException;
import de.hud.sapwatcher.gui.config.main.ConfigMainController;
import de.hud.sapwatcher.gui.mainmenu.MainMenuController;
import de.hud.sapwatcher.helper.ImportsetMetadataAnalyser;
import de.hud.sapwatcher.importer.excel.sap.SapExcelImporter;

public class ApplicationController {

	private static Logger logger = LogManager.getLogger(ApplicationController.class.getName());

	private SapExcelImporter excelImporter = new SapExcelImporter();

	private DataStorage dataStorage = new DataStorage();

	private Stage stage;

	private Pane mainMenuPane;

	private MainMenuController mainMenuController;

	private Scene mainMenuScene;

	private Path lastOpenedFile = null;

	private Config config;

	public ApplicationController() {
	}

	public Stage getStage() {
		return stage;
	}

	public void init() throws IOException {
		try {
			FXMLLoader mainmenuView = new FXMLLoader(MainMenuController.class.getResource("/de/hud/sapwatcher/gui/mainmenu/MainMenuView.fxml"));

			mainMenuPane = mainmenuView.load();
			mainMenuController = mainmenuView.getController();
			mainMenuController.setApplicationController(this);
			// mainMenuController.initializeDynamicContent();

			mainMenuScene = new Scene(mainMenuPane);

		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e);
			System.exit(-1);
		}
	}

	public void displayMainApplication(Stage stage) {
		this.stage = stage;

		displayMainMenu();
	}

	private void displayMainMenu() {
		stage.setScene(mainMenuScene);
		stage.show();
	}

	private Path selectFile() {
		FileChooser fileChooser = new FileChooser();
		if (lastOpenedFile != null) {
			fileChooser.setInitialDirectory(lastOpenedFile.getParent().toFile());
		} else {
			File desktop = new File(System.getProperty("user.home"), "Desktop");
			fileChooser.setInitialDirectory(desktop);
		}
		fileChooser.setTitle("Open Resource File");
		fileChooser.getExtensionFilters().addAll( //
				new ExtensionFilter("Excel", "*.xls", "*.xlsx") //
				);

		File file = fileChooser.showOpenDialog(stage);
		// File file = null;

		if (file == null) {
			return null;
		}

		Path path = Paths.get(file.toURI());

		return path;
	}

	public void openFile() {
		logger.entry();

		mainMenuController.displayWaiting();

		Path file = selectFile();

		if (file == null) {
			mainMenuController.fillFileName("Keine Datei");
		} else {
			loadFile(file);
		}

		logger.exit();
	}

	public void loadFile(Path file) {
		logger.entry();

		mainMenuController.fillFileName(file.getFileName().toString());
		lastOpenedFile = file;

		try {
			List<WorkingTimeEntry> workingTimeEntries = excelImporter.readFile(file);
			dataStorage.storeWorkOrderEntries(workingTimeEntries);

			/* this needs to be done first, because it also resets the filter */
			fillFilterPreselection(dataStorage);

		} catch (InvalidImportFileException e) {
			handleException(e);
		}

		logger.exit();
	}

	private void handleException(InvalidImportFileException e) {
		logger.error(e);
		Alert alert = new Alert(AlertType.ERROR, e.getMessage());
		alert.showAndWait();
	}

	private void fillFilterPreselection(DataStorage dataStorage) {
		logger.entry();

		ImportsetMetadata importsetMetadata = ImportsetMetadataAnalyser.analyseData(dataStorage);
		mainMenuController.fillFilterPreselection(importsetMetadata);

		logger.exit();
	}

	public void fillContentsOnGUI() {

		LocalDate startDate = mainMenuController.getFilterStart();
		LocalDate endDate = mainMenuController.getFilterEnd();

		List<WorkingTimeEntry> filteredEntries = dataStorage.getWorkingTimeEntriesInTimeperiod(startDate, endDate);
		DataStorage filteredDataStorage = new DataStorage();
		filteredDataStorage.storeWorkOrderEntries(filteredEntries);

		displayNewData(filteredDataStorage);
	}

	private void displayNewData(DataStorage dataStorage) {
		logger.entry();

		mainMenuController.displayNewData(dataStorage);

		logger.exit();
	}

	public void displayConfig() throws IOException {
		logger.entry();

		FXMLLoader view = new FXMLLoader(MainMenuController.class.getResource("/de/hud/sapwatcher/gui/config/main/ConfigMainView.fxml"));

		Pane pane = view.load();
		ConfigMainController controller = view.getController();

		controller.setConfig(config);

		final Stage dialogStage = new Stage();
		dialogStage.initModality(Modality.APPLICATION_MODAL);
		dialogStage.initOwner(stage);

		Scene scene = new Scene(pane, 1000, 500);
		dialogStage.setScene(scene);
		dialogStage.show();

		logger.exit();
	}

	public void setConfig(Config config) {
		this.config = config;
		mainMenuController.setConfig(config);
	}

}
