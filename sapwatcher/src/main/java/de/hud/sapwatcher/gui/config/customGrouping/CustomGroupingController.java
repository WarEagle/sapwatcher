package de.hud.sapwatcher.gui.config.customGrouping;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.FloatStringConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.CustomGrouping;

public class CustomGroupingController implements Initializable {

	private static Logger logger = LogManager.getLogger(CustomGroupingController.class.getName());

	@FXML
	private TableColumn<CustomGrouping, String> columnProjectDescription;

	@FXML
	private TableColumn<CustomGrouping, String> columnProjectname;

	@FXML
	private TableColumn<CustomGrouping, String> columnTask;

	@FXML
	private TableColumn<CustomGrouping, String> columnName;

	@FXML
	private TableColumn<CustomGrouping, String> columnEmployee;

	@FXML
	private TableColumn<CustomGrouping, Float> columnBudget;

	@FXML
	private TableColumn<CustomGrouping, BooleanProperty> columnActive;

	@FXML
	private Button buttonDeleteEntry;

	@FXML
	private Button buttonNewEntry;
	
    @FXML
    private Button buttonDoubleEntry;

	@FXML
	private TableView<CustomGrouping> tableView;

	private ObservableList<CustomGrouping> data = FXCollections.observableArrayList();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		logger.entry();

		initColumnName();
		initColumnBudget();
		initColumnDescription();
		initColumnEmployee();
		initColumnProject();
		initColumnTask();
		initColumnActive();

		// Button
		buttonDeleteEntry.disableProperty().bind(tableView.selectionModelProperty().get().selectedItemProperty().isNull());
		buttonDoubleEntry.disableProperty().bind(tableView.selectionModelProperty().get().selectedItemProperty().isNull());

		logger.exit();
	}

	private void initColumnActive() {
		columnActive.setCellFactory(p -> new CheckBoxTableCell<>());
		columnActive.setCellValueFactory(new PropertyValueFactory<>("active"));
	}

	private void initColumnTask() {
		columnTask.setCellValueFactory(new PropertyValueFactory<>("filterTask"));
		columnTask.setCellFactory(TextFieldTableCell.forTableColumn());

		columnTask.setOnEditCommit(t -> t.getTableView().getItems().get(t.getTablePosition().getRow()).setFilterTask(t.getNewValue()));
	}

	private void initColumnProject() {
		columnProjectname.setCellValueFactory(new PropertyValueFactory<>("filterProjectname"));
		columnProjectname.setCellFactory(TextFieldTableCell.forTableColumn());
		columnProjectname.setOnEditCommit(t -> t.getTableView().getItems().get(t.getTablePosition().getRow()).setFilterProjectName(t.getNewValue()));
	}

	private void initColumnEmployee() {
		columnEmployee.setCellValueFactory(new PropertyValueFactory<>("filterEmployee"));
		columnEmployee.setCellFactory(TextFieldTableCell.forTableColumn());
		columnEmployee.setOnEditCommit(t -> t.getTableView().getItems().get(t.getTablePosition().getRow()).setFilterEmployee(t.getNewValue()));
	}

	private void initColumnDescription() {
		columnProjectDescription.setCellValueFactory(new PropertyValueFactory<>("filterDescription"));
		columnProjectDescription.setCellFactory(TextFieldTableCell.forTableColumn());
		columnProjectDescription.setOnEditCommit(t -> t.getTableView().getItems().get(t.getTablePosition().getRow()).setFilterDescription(t.getNewValue()));
	}

	private void initColumnBudget() {
		columnBudget.setCellValueFactory(new PropertyValueFactory<>("budget"));
		columnBudget.setCellFactory(TextFieldTableCell.forTableColumn(new FloatStringConverter()));
		columnBudget.setOnEditCommit(t -> t.getTableView().getItems().get(t.getTablePosition().getRow()).setBudget(t.getNewValue()));
	}

	private void initColumnName() {
		columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
		columnName.setCellFactory(TextFieldTableCell.forTableColumn());
		columnName.setOnEditCommit(t -> t.getTableView().getItems().get(t.getTablePosition().getRow()).setName(t.getNewValue()));
	}

	@FXML
	void createNewEntry(ActionEvent event) {
		logger.entry();

		data.add(new CustomGrouping("Neu", true, ".*", ".*", ".*", ".*", 0f));

		logger.exit();
	}

	@FXML
	void doubleSelectedEntry(ActionEvent event) {
		logger.entry();

		ReadOnlyObjectProperty<CustomGrouping> selectedItem = tableView.selectionModelProperty().getValue().selectedItemProperty();
		data.add(clonedCopy(selectedItem.getValue()));

		logger.exit();
	}

	private CustomGrouping clonedCopy(CustomGrouping original) {
		CustomGrouping rc = new CustomGrouping("Neu " + original.getName(), original.isActive(), original.getFilterProjectName(),
				original.getFilterDescription(), original.getFilterTask(), original.getEmployee(), original.getBudget());
		return rc;
	}

	@FXML
	void deleteSelectedEntries(ActionEvent event) {
		logger.entry();

		ReadOnlyObjectProperty<CustomGrouping> selectedItem = tableView.selectionModelProperty().getValue().selectedItemProperty();
		data.remove(selectedItem.getValue());

		logger.exit();
	}

	public void setData(ObservableList<CustomGrouping> data) {
		logger.entry();

		this.data = data;
		tableView.setItems(data);

		logger.exit();
	}

	public ObservableList<CustomGrouping> getData() {
		return data;
	}

}
