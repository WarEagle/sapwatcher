package de.hud.sapwatcher.gui.filter;

import java.time.LocalDate;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.HBox;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.ApplicationController;
import de.hud.sapwatcher.data.ImportsetMetadata;

public class FilterController {

	private static Logger logger = LogManager.getLogger(FilterController.class.getName());

	@FXML
	private HBox overviewPane;

	@FXML
	private DatePicker filterStartDateDP;

	@FXML
	private DatePicker filterEndDateDP;

	private ApplicationController applicationController;

	private boolean isAutofilling = false;

	@FXML
	void initialize() {
		logger.entry();

		filterStartDateDP.setOnAction(event -> {
			if (!isAutofilling) {
				applicationController.fillContentsOnGUI();
			}
		});
		//
		filterEndDateDP.setOnAction(event -> {
			if (!isAutofilling) {
				applicationController.fillContentsOnGUI();
			}
		});

		logger.exit();
	}

	public void fillFilterPreselection(ImportsetMetadata importsetMetadata) {
		logger.entry(importsetMetadata);

		isAutofilling = true;
		filterStartDateDP.setValue(importsetMetadata.getStartTime().toLocalDate());
		filterEndDateDP.setValue(importsetMetadata.getEndTime().toLocalDate());
		isAutofilling = false;

		logger.exit();
	}

	public LocalDate getFilterStart() {
		return filterStartDateDP.getValue();
	}

	public LocalDate getFilterEnd() {
		return filterEndDateDP.getValue();
	}

	public void setApplicationController(ApplicationController applicationController) {
		this.applicationController = applicationController;

	}
}
