package de.hud.sapwatcher.gui.mainmenu.summary;

import de.hud.sapwatcher.data.OutputDataStructure;
import de.hud.sapwatcher.exporter.ascii.AsciiOutputter;
import de.hud.sapwatcher.exporter.excel.SapExcelOutputterImpl;
import de.hud.sapwatcher.processor.summary.AbstractDataGenerator;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Border;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class SummaryController implements Initializable {
	@FXML
	private TextArea textTA;

	@FXML
	private PieChart overviewPC;

	@FXML
	private ToggleButton showTasksTB;

	@FXML
	private ToggleButton showEmployeeTB;

	@FXML
	private ToggleButton showDescriptionTB;

	private AsciiOutputter asciiOutputter;

	private AbstractDataGenerator generator;

	private final SapExcelOutputterImpl almSapExcelOutputter = new SapExcelOutputterImpl();


	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		overviewPC.setStartAngle(90);
		overviewPC.setClockwise(true);
		overviewPC.setBorder(Border.EMPTY);
		overviewPC.setLabelsVisible(false);

		showDescriptionTB.managedProperty().bindBidirectional(showDescriptionTB.visibleProperty());
		showEmployeeTB.managedProperty().bindBidirectional(showEmployeeTB.visibleProperty());
		showTasksTB.managedProperty().bindBidirectional(showTasksTB.visibleProperty());

	}

	public void disableEmployeeDisplay() {
		showEmployeeTB.setVisible(false);
	}

	public void disableDescriptionDisplay() {
		showDescriptionTB.setVisible(false);
	}

	public void disableTasksDisplay() {
		showTasksTB.setVisible(false);
	}

	public void setAsciiOutputter(AsciiOutputter asciiOutputter) {
		this.asciiOutputter = asciiOutputter;
	}

	public void setGenerator(AbstractDataGenerator generator) {
		this.generator = generator;
		generator.getOutputTasksProperty().bind(showTasksTB.selectedProperty());
		showTasksTB.selectedProperty().addListener((observable, oldValue, newValue) -> {
            refresh();
        });

		generator.getOutputEmployeeProperty().bind(showEmployeeTB.selectedProperty());
		showEmployeeTB.selectedProperty().addListener((observable, oldValue, newValue) -> {
            refresh();
        });

		generator.getOutputDescriptionProperty().bind(showDescriptionTB.selectedProperty());
		showDescriptionTB.selectedProperty().addListener((observable, oldValue, newValue) -> {
            refresh();
        });
	}

	private void refreshPieChart(List<OutputDataStructure> dataStructure) {
		ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

		Tooltip t;
		for (OutputDataStructure outputDataStructure : dataStructure) {
			PieChart.Data data = new PieChart.Data(outputDataStructure.getSectionName(), outputDataStructure.getTotalSumHours());

			pieChartData.add(data);
		}

		overviewPC.setData(pieChartData);

		for (final PieChart.Data data : overviewPC.getData()) {
			t = new Tooltip(data.getName() + " " + data.getPieValue() + "");
			Tooltip.install(data.getNode(), t);
		}
	}

	public void refresh() {
		List<OutputDataStructure> dataStructure = generator.fillDataStructure();
		String text = asciiOutputter.toReadableText(dataStructure);
		Platform.runLater(() -> {
			refreshPieChart(dataStructure);
			textTA.setText(text);
		});
	}

	public void startWaiting() {
		String waitingText = "Bitte warten...";
		Platform.runLater(() -> {
			textTA.setText(waitingText);
		});
	}

	@FXML
	void exportToExcel(ActionEvent event) {
		List<OutputDataStructure> dataStructure = generator.fillDataStructure();
		try {
			XSSFWorkbook workbook = almSapExcelOutputter.toWorkbook(dataStructure);
			String filename = System.getProperty("user.home") + "/Desktop/SapWatcher_" + System.currentTimeMillis() + ".xlsx";
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filename));
			workbook.write(bos);
			bos.flush();
			bos.close();
			Alert alert = new Alert(Alert.AlertType.INFORMATION, "Datei unter '" + filename + "' abgelegt.");
			alert.showAndWait();
		} catch (Exception e) {
			Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
			alert.showAndWait();
			//logger.error(e);
		}
	}

}
