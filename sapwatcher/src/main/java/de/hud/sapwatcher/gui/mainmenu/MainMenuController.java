package de.hud.sapwatcher.gui.mainmenu;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.ApplicationController;
import de.hud.sapwatcher.data.Config;
import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.data.ImportsetMetadata;
import de.hud.sapwatcher.data.OutputDataStructure;
import de.hud.sapwatcher.exporter.ascii.AsciiOutputter;
import de.hud.sapwatcher.exporter.ascii.AsciiOutputterImpl;
import de.hud.sapwatcher.gui.alm.AlmCompareController;
import de.hud.sapwatcher.gui.filter.FilterController;
import de.hud.sapwatcher.gui.mainmenu.analysis.AnalysisController;
import de.hud.sapwatcher.gui.mainmenu.summary.SummaryController;
import de.hud.sapwatcher.gui.metainfo.MetaInfoController;
import de.hud.sapwatcher.helper.ImportsetMetadataAnalyser;
import de.hud.sapwatcher.helper.PriceCalculator;
import de.hud.sapwatcher.processor.alm.AlmCompareGenerator;
import de.hud.sapwatcher.processor.summary.AllTasksGenerator;
import de.hud.sapwatcher.processor.summary.CustomGroupingGenerator;
import de.hud.sapwatcher.processor.summary.EmployeePerProjectGenerator;
import de.hud.sapwatcher.processor.summary.SummaryGenerator;
import de.hud.sapwatcher.processor.summary.TasksPerProjectGenerator;

public class MainMenuController {
//TODO add ProjectPerEmployee
	private static Logger logger = LogManager.getLogger(MainMenuController.class.getName());

	@FXML
	private ToggleButton displayInfoButton;

	@FXML
	private ToggleButton displayFilterButton;

	@FXML
	private Button openFileButton;

	@FXML
	private Button displayConfigButton;

	@FXML
	private HBox overviewPane;

	@FXML
	private HBox filterPane;

	@FXML
	private TextField currentFile;

	@FXML
	private TextArea customGroupingTA;

	@FXML
	private MetaInfoController metaInfoController;

	@FXML
	private SummaryController summaryController;

	@FXML
	private SummaryController tasksByProjectController;

	@FXML
	private SummaryController allTasksController;

	@FXML
	private SummaryController employeeByProjectController;

	@FXML
	private AnalysisController analysisController;

	@FXML
	private AlmCompareController almCompareController;

	@FXML
	private FilterController filterController;

	private ApplicationController applicationController;

	private SummaryGenerator summaryGenerator = new SummaryGenerator();

	private TasksPerProjectGenerator tasksByProjectGenerator = new TasksPerProjectGenerator();

	private PriceCalculator priceCalculator = new PriceCalculator();

	private AllTasksGenerator allTasksGenerator = new AllTasksGenerator();

	private AlmCompareGenerator almCompareGenerator = new AlmCompareGenerator();

	private CustomGroupingGenerator customGroupingGenerator = new CustomGroupingGenerator(priceCalculator);

	private EmployeePerProjectGenerator employeePerProjectGenerator = new EmployeePerProjectGenerator();

	@FXML
	void initialize() {
		logger.entry();

		overviewPane.visibleProperty().bind(displayInfoButton.selectedProperty());
		overviewPane.managedProperty().bind(displayInfoButton.selectedProperty());

		filterPane.visibleProperty().bind(displayFilterButton.selectedProperty());
		filterPane.managedProperty().bind(displayFilterButton.selectedProperty());

		initializeControllers();

		logger.exit();
	}

	private void initializeControllers() {

		summaryController.setAsciiOutputter(new AsciiOutputterImpl());
		summaryController.setGenerator(summaryGenerator);
		summaryController.disableEmployeeDisplay();
		summaryController.disableDescriptionDisplay();

		tasksByProjectController.setAsciiOutputter(new AsciiOutputterImpl());
		tasksByProjectController.setGenerator(tasksByProjectGenerator);
		tasksByProjectController.disableDescriptionDisplay();

		employeeByProjectController.setAsciiOutputter(new AsciiOutputterImpl());
		employeeByProjectController.setGenerator(employeePerProjectGenerator);
		employeeByProjectController.disableEmployeeDisplay();

		allTasksController.setAsciiOutputter(new AsciiOutputterImpl());
		allTasksController.setGenerator(allTasksGenerator);
		allTasksController.disableDescriptionDisplay();
		allTasksController.disableTasksDisplay();

		almCompareController.setAlmCompareGenerator(almCompareGenerator);

	}

	public void setApplicationController(ApplicationController applicationController) {
		this.applicationController = applicationController;
		almCompareController.setApplicationController(applicationController);
		filterController.setApplicationController(applicationController);
	}

	@FXML
	void displayConfig(ActionEvent event) throws IOException {
		logger.entry(event);

		applicationController.displayConfig();

		logger.exit();
	}

	@FXML
	void openFile(ActionEvent event) {
		logger.entry(event);

		applicationController.openFile();
		applicationController.fillContentsOnGUI();

		logger.exit();
	}

	public void fillFileName(String text) {
		currentFile.setText(text);
	}

	public void fillFilterPreselection(ImportsetMetadata importsetMetadata) {
		filterController.fillFilterPreselection(importsetMetadata);
	}

	private void fillMetaData(ImportsetMetadata importsetMetadata) {
		metaInfoController.fillMetaData(importsetMetadata);
	}

	public LocalDate getFilterStart() {
		return filterController.getFilterStart();
	}

	public LocalDate getFilterEnd() {
		return filterController.getFilterEnd();
	}

	// public void fillContentsOnGUI() {
	// applicationController.fillContentsOnGUI();
	// }

	public void displayNewData(DataStorage dataStorage) {
		logger.entry();

		summaryGenerator.setDataStorage(dataStorage);
		summaryController.refresh();

		employeePerProjectGenerator.setDataStorage(dataStorage);
		employeeByProjectController.refresh();

		allTasksGenerator.setDataStorage(dataStorage);
		allTasksController.refresh();

		tasksByProjectGenerator.setDataStorage(dataStorage);
		tasksByProjectController.refresh();

		analysisController.refreshAnalysis(dataStorage);
		
		almCompareGenerator.setDataStorageSap(dataStorage);
		almCompareController.refresh();

		// TODO
		fillCustomGrouping(dataStorage);

		// TODO
		fillMetaData(dataStorage);

		logger.exit();
	}

	private void fillCustomGrouping(DataStorage dataStorage) {
		logger.entry();

		Platform.runLater(() -> {
			// TODO
			AsciiOutputter asciiOutputter = new AsciiOutputterImpl();
			customGroupingGenerator.setDataStorage(dataStorage);
			List<OutputDataStructure> dataStructure = customGroupingGenerator.fillDataStructure();
			String text = asciiOutputter.toReadableText(dataStructure);
			customGroupingTA.setText(text);
		});

		logger.exit();
	}

	private void fillMetaData(DataStorage dataStorage) {
		logger.entry();

		Platform.runLater(() -> {
			ImportsetMetadata importsetMetadata = ImportsetMetadataAnalyser.analyseData(dataStorage);
			fillMetaData(importsetMetadata);
		});

		logger.exit();
	}

	public void setConfig(Config config) {
		logger.entry();

		summaryGenerator.setConfig(config);
		tasksByProjectGenerator.setConfig(config);
		employeePerProjectGenerator.setConfig(config);
		customGroupingGenerator.setConfig(config);
		allTasksGenerator.setConfig(config);
		priceCalculator.setPrices(config.getHourlyRatesAsMap());

		logger.exit();
	}

	public void displayWaiting() {
		String waitingText = "Bitte warten...";
		// TODO remove runlater when everything is done in it's own thread
		Platform.runLater(() -> {
			customGroupingTA.setText(waitingText);
			analysisController.startWaiting();
			tasksByProjectController.startWaiting();
			employeeByProjectController.startWaiting();
			allTasksController.startWaiting();
			summaryController.startWaiting();
		});
	}

}
