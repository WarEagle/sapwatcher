package de.hud.sapwatcher.gui.mainmenu.analysis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.data.WorkingTimeEntry;
import de.hud.sapwatcher.helper.StringHelper;

class TasksGrouper {
	private DataStorage dataStorage;

	private Map<String, Series<String, Float>> seriesForGraph = new TreeMap<>();

	private boolean doShortNames;

	public TasksGrouper(DataStorage dataStorage) {
		this.dataStorage = dataStorage;

		doShortNames = dataStorage.getListNamesEmployee().size() > 4;
	}

	public List<Series<String, Float>> generateSeries() {
		seriesForGraph = new TreeMap<>();

		List<Series<String, Float>> rc = new ArrayList<>();

		Map<String, Map<String, Float>> groupByTask = groupByTask();

		for (String employee : groupByTask.keySet()) {
			fillTasksForEmployee(groupByTask, employee);
		}

		for (String task : seriesForGraph.keySet()) {
			Series<String, Float> series = seriesForGraph.get(task);
			series.setName(task);
			rc.add(series);
		}
		return rc;
	}

	private void fillTasksForEmployee(Map<String, Map<String, Float>> groupByTask, String employee) {
		Map<String, Float> entry = groupByTask.get(employee);

		Data<String, Float> data;

		for (String task : entry.keySet()) {
			String employeeDescription = (doShortNames || employee.length() > 30) ? StringHelper.shortenName(employee) : employee;
			data = new Data<>(employeeDescription, entry.get(task));

			Series<String, Float> series = getOrCreateSeries(task);
			series.getData().add(data);
		}
	}

	private Series<String, Float> getOrCreateSeries(String task) {

		if (!seriesForGraph.containsKey(task)) {
			Series<String, Float> series = new Series<>();
			seriesForGraph.put(task, series);
		}
		return seriesForGraph.get(task);
	}

	private Map<String, Map<String, Float>> groupByTask() {
		Map<String, Map<String, Float>> employeeData = new TreeMap<>();

		List<String> listNamesEmployee = dataStorage.getListNamesEmployee();
		for (String employee : listNamesEmployee) {
			Map<String, Float> data = groupByTaskForEmployee(employee);
			employeeData.put(employee, data);
		}

		return employeeData;
	}

	private Map<String, Float> groupByTaskForEmployee(String employee) {
		Map<String, Float> timeForTasks = new TreeMap<>();

		for (WorkingTimeEntry workingTimeEntry : dataStorage.getWorkingTimeEntriesForEmployee(employee)) {
			fillDetailsForTask(workingTimeEntry, timeForTasks);
		}

		return timeForTasks;
	}

	private void fillDetailsForTask(WorkingTimeEntry workingTimeEntry, Map<String, Float> timeForTasks) {

		String task = workingTimeEntry.getTask();
		if (task.trim().isEmpty()) {
			task = "<LEER>";
		}
		if (!timeForTasks.containsKey(task)) {
			timeForTasks.put(task, 0f);
		}
		float existing = timeForTasks.get(task);
		timeForTasks.put(task, existing + workingTimeEntry.getDurationInHours());
	}
}
