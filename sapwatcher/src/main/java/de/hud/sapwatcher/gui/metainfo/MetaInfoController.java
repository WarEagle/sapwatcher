package de.hud.sapwatcher.gui.metainfo;

import java.time.format.DateTimeFormatter;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.ImportsetMetadata;

public class MetaInfoController {

	private static Logger logger = LogManager.getLogger(MetaInfoController.class.getName());

	@FXML
	private Label valueNumberEntries;

	@FXML
	private Label valueNumberEmployee;

	@FXML
	private Label valueNumberProjects;

	@FXML
	private Label valueCountHours;

	@FXML
	private Label valueStarttime;

	@FXML
	private Label valueEndtime;

	@FXML
	private GridPane overviewPane;

	@FXML
	void initialize() {
		logger.entry();

		logger.exit();
	}

	public void fillMetaData(ImportsetMetadata importsetMetadata) {

		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE;

		valueStarttime.setText(importsetMetadata.getStartTime().format(formatter));
		valueEndtime.setText(importsetMetadata.getEndTime().format(formatter));
		valueCountHours.setText(String.format("%8.2f", importsetMetadata.getCountHours()));
		valueNumberEmployee.setText(Integer.toString(importsetMetadata.getNumberEmployee()));
		valueNumberEntries.setText(Integer.toString(importsetMetadata.getNumberEntries()));
		valueNumberProjects.setText(Integer.toString(importsetMetadata.getNumberProjects()));
	}

}
