package de.hud.sapwatcher.gui.mainmenu.analysis;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.exporter.ascii.AsciiOutputter;
import de.hud.sapwatcher.exporter.ascii.AsciiOutputterImpl;
import de.hud.sapwatcher.exporter.gui.average.EmployeeWorkingTimePane;
import de.hud.sapwatcher.exporter.gui.workingtime.DayOfMonthPaneGenerator;
import de.hud.sapwatcher.exporter.gui.workingtime.LocalDatePaneGenerator;
import de.hud.sapwatcher.processor.analysis.AverageWorkingtime;
import de.hud.sapwatcher.processor.analysis.SpecialWorkingTimeEntries;

public class AnalysisController implements Initializable {

	private static Logger logger = LogManager.getLogger(AnalysisController.class.getName());

	private AsciiOutputter asciiOutputter = new AsciiOutputterImpl();

	@FXML
	private ScrollPane analysisScrollPane;

	@FXML
	private TextArea analysisTA;

	@FXML
	private VBox analysisPane;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		DoubleProperty wProperty = new SimpleDoubleProperty();
		wProperty.bind(analysisPane.widthProperty()); // bind to Hbox width chnages
		wProperty.addListener((ov, t, t1) -> {
            // when ever Hbox width changes set ScrollPane Hvalue
            analysisScrollPane.setHvalue(analysisScrollPane.getHmax());
        });

	}

	public void refreshAnalysis(DataStorage dataStorage) {
		logger.entry();

		Platform.runLater(() -> {
			analysisPane.getChildren().clear();

			fillWorkingDayDays(dataStorage);
			fillMissingEntries(dataStorage);
			fillReqDefOtherGraph(dataStorage);
			fillTasksGraph(dataStorage);
		});

		logger.exit();
	}

	private void fillReqDefOtherGraph(DataStorage dataStorage) {
		ReqDefOtherGrouper reqDefOtherGrouper = new ReqDefOtherGrouper(dataStorage);
		List<Series<String, Float>> generateSeries = reqDefOtherGrouper.generateSeries();
		
		EmployeeWorkingTimePane employeeWorkingTimePane = new EmployeeWorkingTimePane("Bereich");
		employeeWorkingTimePane.setData(FXCollections.observableList(generateSeries));
		
		analysisPane.getChildren().add(employeeWorkingTimePane);
	}

	private void fillTasksGraph(DataStorage dataStorage) {
		TasksGrouper tasksGrouper = new TasksGrouper(dataStorage);
		List<Series<String, Float>> generateSeries = tasksGrouper.generateSeries();
		
		EmployeeWorkingTimePane employeeWorkingTimePane = new EmployeeWorkingTimePane("Tätigkeiten");
		employeeWorkingTimePane.setData(FXCollections.observableList(generateSeries));
		
		analysisPane.getChildren().add(employeeWorkingTimePane);
	}
	
	private void fillMissingEntries(DataStorage dataStorage) {
		StringBuffer text = new StringBuffer();
		SpecialWorkingTimeEntries specialWorkingTimeEntries = new SpecialWorkingTimeEntries(dataStorage);
		text.append("Leere Tasks:");
		text.append("\n");
		text.append(asciiOutputter.toReadableEmailText(specialWorkingTimeEntries.findAllEmptyTasks()));
		text.append("\n");
		text.append("\n");
		text.append("\n");
		text.append("\n");
		text.append("Leere Beschreibung:");
		text.append("\n");
		text.append(asciiOutputter.toReadableEmailText(specialWorkingTimeEntries.findAllEmptyDescriptions()));
		text.append("\n");

		analysisTA.setText(text.toString());
	}

	private void fillWorkingDayDays(DataStorage dataStorage) {
		AverageWorkingtime generator = new AverageWorkingtime(dataStorage);

		DayOfMonthPaneGenerator dayOfWeekPane = new DayOfMonthPaneGenerator(generator.getTimePerWeekday(), "Wochenschnitt");
		LocalDatePaneGenerator localDatePaneGenerator = new LocalDatePaneGenerator(generator.aggregateByDayOfMonth(dataStorage), "Tagesschnitt");

		analysisPane.getChildren().add(dayOfWeekPane.generatePane());
		analysisPane.getChildren().add(localDatePaneGenerator.generatePane());
	}

	public void startWaiting() {
		String waitingText = "Bitte warten...";
		Platform.runLater(() -> {
			analysisTA.setText(waitingText);
		});
	}

}
