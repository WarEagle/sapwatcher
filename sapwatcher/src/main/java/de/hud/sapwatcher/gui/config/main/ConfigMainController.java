package de.hud.sapwatcher.gui.config.main;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.Config;
import de.hud.sapwatcher.gui.config.customGrouping.CustomGroupingController;
import de.hud.sapwatcher.gui.config.hourlyRate.HourlyRateController;

public class ConfigMainController implements Initializable {

	private static Logger logger = LogManager.getLogger(ConfigMainController.class.getName());

	@FXML
	private Button buttonSave;

	@FXML
	private Button buttonReset;

	@FXML
	private CustomGroupingController customGroupingController;

	@FXML
	private HourlyRateController hourlyRateController;

	private Config config;

	public void setConfig(Config config) {
		this.config = config;

		customGroupingController.setData(FXCollections.observableArrayList(config.getCustomGroupings()));
		hourlyRateController.setData(FXCollections.observableArrayList(config.getHourlyRates()));
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		logger.entry();

		// TODO
		buttonReset.setDisable(true);

		logger.exit();
	}

	@FXML
	void resetConfig(ActionEvent event) {
		logger.entry();

		logger.exit();
	}

	@FXML
	void saveConfig(ActionEvent event) {
		logger.entry();

		config.setCustomGroupings(new ArrayList<>(customGroupingController.getData()));
		config.setHourlyRates(new ArrayList<>(hourlyRateController.getData()));
		
		try {
			config.writeToFile();
		} catch (JAXBException | IOException e) {
			e.printStackTrace();
			logger.error(e);
		}
		
		logger.exit();
	}

}