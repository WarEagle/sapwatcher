package de.hud.sapwatcher.gui.config.hourlyRate;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.HourlyRate;

public class HourlyRateController implements Initializable {

	private static Logger logger = LogManager.getLogger(HourlyRateController.class.getName());

	@FXML
	private TableColumn<HourlyRate, String> columnName;

	@FXML
	private TableColumn<HourlyRate, Float> columnRate;

	@FXML
	private Button buttonDeleteEntry;

	@FXML
	private Button buttonNewEntry;

	@FXML
	private TableView<HourlyRate> tableView;

	private ObservableList<HourlyRate> data = FXCollections.observableArrayList();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		logger.entry();

		columnName.setCellValueFactory(new PropertyValueFactory<>("soldService"));
		columnName.setCellFactory(TextFieldTableCell.forTableColumn());
		columnName.setOnEditCommit(t -> t.getTableView().getItems().get(t.getTablePosition().getRow()).setSoldService(t.getNewValue()));

		columnRate.setCellValueFactory(new PropertyValueFactory<>("rate"));
		columnRate.setOnEditCommit(t -> t.getTableView().getItems().get(t.getTablePosition().getRow()).setRate(t.getNewValue()));

		buttonDeleteEntry.disableProperty().bind(tableView.selectionModelProperty().get().selectedItemProperty().isNull());

		logger.exit();
	}

	@FXML
	void createNewEntry(ActionEvent event) {
		logger.entry();

		data.add(new HourlyRate("Neu", 0f));

		logger.exit();
	}

	@FXML
	void deleteSelectedEntries(ActionEvent event) {
		logger.entry();

		ReadOnlyObjectProperty<HourlyRate> selectedItem = tableView.selectionModelProperty().getValue().selectedItemProperty();
		data.remove(selectedItem.getValue());

		logger.exit();
	}

	public void setData(ObservableList<HourlyRate> data) {
		logger.entry();

		this.data = data;
		tableView.setItems(data);

		logger.exit();
	}

	public ObservableList<HourlyRate> getData() {
		return data;
	}

}
