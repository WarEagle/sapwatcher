package de.hud.sapwatcher.gui.alm;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.ApplicationController;
import de.hud.sapwatcher.data.alm.AlmSapCompareEntry;
import de.hud.sapwatcher.data.alm.AlmSapCompareOutputEntry;
import de.hud.sapwatcher.exception.InvalidImportFileException;
import de.hud.sapwatcher.exporter.ascii.AlmSapAsciiOutputter;
import de.hud.sapwatcher.exporter.excel.AlmSapExcelOutputter;
import de.hud.sapwatcher.importer.excel.alm.AlmExcelImporter;
import de.hud.sapwatcher.processor.alm.AlmCompareGenerator;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class AlmCompareController implements Initializable {

	private static Logger logger = LogManager.getLogger(AlmCompareController.class.getName());

	private ApplicationController applicationController;

	private Path lastOpenedFile = null;

	private AlmCompareGenerator almCompareGenerator;

	private final AlmSapAsciiOutputter almSapAsciiOutputter = new AlmSapAsciiOutputter();

	private final AlmExcelImporter almExcelImporter = new AlmExcelImporter();

	private final AlmSapExcelOutputter almSapExcelOutputter = new AlmSapExcelOutputter();

	@FXML
	private TextArea textTA;

	@FXML
	private ToggleButton showEmployeeTB;

	@FXML
	private ToggleButton aggregateDefectsToRequirementsTB;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}

	public void startWaiting() {
		String waitingText = "Bitte warten...";
		Platform.runLater(() -> {
			textTA.setText(waitingText);
		});
	}

	public void setAlmCompareGenerator(AlmCompareGenerator almCompareGenerator) {
		this.almCompareGenerator = almCompareGenerator;

		aggregateDefectsToRequirementsTB.selectedProperty().addListener((observable, oldValue, newValue) -> {
            refresh();
        });
	}

	public void refresh() {
		logger.entry();
		List<AlmSapCompareOutputEntry> entries = almCompareGenerator.compareEntries(aggregateDefectsToRequirementsTB.isSelected());
		Platform.runLater(() -> {
			textTA.setText(almSapAsciiOutputter.createOutput(entries));
		});
		logger.exit();
	}

	@FXML
	void loadAlmFile(ActionEvent event) {
		Path file = selectFile();

		if (file != null) {
			loadFile(file);
		}
	}

	@FXML
	void exportToExcel(ActionEvent event) {
		List<AlmSapCompareOutputEntry> entries = almCompareGenerator.compareEntries(aggregateDefectsToRequirementsTB.isSelected());
		try {
			String filename = almSapExcelOutputter.createOutput(entries);
			Alert alert = new Alert(AlertType.INFORMATION, "Datei unter '" + filename + "' abgelegt.");
			alert.showAndWait();
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR, e.getMessage());
			alert.showAndWait();
			logger.error(e);
		}
	}

	private void loadFile(Path file) {
		try {
			List<AlmSapCompareEntry> entriesAlm = almExcelImporter.readFile(file);
			almCompareGenerator.setAlmData(entriesAlm);
			refresh();
		} catch (InvalidImportFileException e) {
			Alert alert = new Alert(AlertType.ERROR, e.getMessage());
			alert.showAndWait();
			logger.error(e);
		}
	}

	private Path selectFile() {
		FileChooser fileChooser = new FileChooser();
		if (lastOpenedFile != null) {
			fileChooser.setInitialDirectory(lastOpenedFile.getParent().toFile());
		} else {
			File desktop = new File(System.getProperty("user.home"), "Desktop");
			fileChooser.setInitialDirectory(desktop);
		}
		fileChooser.setTitle("Open ALM File");
		fileChooser.getExtensionFilters().addAll( //
				new ExtensionFilter("Excel", "*.xls", "*.xlsx") //
				);

		File file = fileChooser.showOpenDialog(applicationController.getStage());

		if (file == null) {
			return null;
		}

		Path path = Paths.get(file.toURI());

		return path;
	}

	public void setApplicationController(ApplicationController applicationController) {
		this.applicationController = applicationController;
	}

}
