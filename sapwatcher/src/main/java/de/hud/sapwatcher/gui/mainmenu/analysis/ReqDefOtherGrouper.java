package de.hud.sapwatcher.gui.mainmenu.analysis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Tooltip;

import org.apache.commons.lang3.tuple.MutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.data.WorkingTimeEntry;
import de.hud.sapwatcher.helper.IdFinder;
import de.hud.sapwatcher.helper.StringHelper;

class ReqDefOtherGrouper {
	private DataStorage dataStorage;
	boolean doShortNames;

	public ReqDefOtherGrouper(DataStorage dataStorage) {
		this.dataStorage = dataStorage;
		doShortNames = dataStorage.getListNamesEmployee().size() > 4;
	}

	public List<Series<String, Float>> generateSeries() {
		List<Series<String, Float>> rc = new ArrayList<>();

		Map<String, Triple<Float, Float, Float>> groupByReqDefOther = groupByReqDefOther();
		Series<String, Float> seriesReq = new Series<>();
		seriesReq.setName(IdFinder.TEXT_REQUIREMENT);

		Series<String, Float> seriesDef = new Series<>();
		seriesDef.setName(IdFinder.TEXT_DEFECT);

		Series<String, Float> seriesOther = new Series<>();
		seriesOther.setName(IdFinder.TEXT_OTHER);

		for (String employee : groupByReqDefOther.keySet()) {
			Triple<Float, Float, Float> triple = groupByReqDefOther.get(employee);

			Data<String, Float> data;
			Tooltip toolTip;
			String employeeDescription = (doShortNames || employee.length() > 30) ? StringHelper.shortenName(employee) : employee;

			data = new Data<>(employeeDescription, triple.getRight());
			toolTip = new Tooltip(IdFinder.TEXT_OTHER);
			Tooltip.install(data.getNode(), toolTip);
			seriesOther.getData().add(data);

			data = new Data<>(employeeDescription, triple.getMiddle());
			toolTip = new Tooltip(IdFinder.TEXT_DEFECT);
			seriesDef.getData().add(data);
			Tooltip.install(data.getNode(), toolTip);

			data = new Data<>(employeeDescription, triple.getLeft());
			toolTip = new Tooltip(IdFinder.TEXT_REQUIREMENT);
			seriesReq.getData().add(data);
			Tooltip.install(data.getNode(), toolTip);

		}
		rc.add(seriesOther);
		rc.add(seriesDef);
		rc.add(seriesReq);

		return rc;
	}

	private Map<String, Triple<Float, Float, Float>> groupByReqDefOther() {
		Map<String, Triple<Float, Float, Float>> rc = new TreeMap<>();

		List<String> listNamesEmployee = dataStorage.getListNamesEmployee();
		for (String employee : listNamesEmployee) {
			Triple<Float, Float, Float> data = groupByReqDefOtherForEmployee(employee);
			rc.put(employee, data);
		}

		return rc;
	}

	private Triple<Float, Float, Float> groupByReqDefOtherForEmployee(String employee) {
		MutableTriple<Float, Float, Float> rc = new MutableTriple<>(0f, 0f, 0f);

		for (WorkingTimeEntry workingTimeEntry : dataStorage.getWorkingTimeEntriesForEmployee(employee)) {
			fillDetailsForReqDefOther(workingTimeEntry, rc);
		}

		return rc;
	}

	private void fillDetailsForReqDefOther(WorkingTimeEntry workingTimeEntry, MutableTriple<Float, Float, Float> data) {
		Map<String, Float> tmpTask2time = workingTimeEntry.getSplittedDescription();

		for (String task : tmpTask2time.keySet()) {
			if (task.startsWith(IdFinder.TEXT_REQUIREMENT)) {
				data.setLeft(data.getLeft() + tmpTask2time.get(task));
			} else if (task.startsWith(IdFinder.TEXT_DEFECT)) {
				data.setMiddle(data.getMiddle() + tmpTask2time.get(task));
			} else {
				data.setRight(data.getRight() + tmpTask2time.get(task));
			}
		}
	}
}
