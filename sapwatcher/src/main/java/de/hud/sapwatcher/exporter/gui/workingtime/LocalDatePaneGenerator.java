package de.hud.sapwatcher.exporter.gui.workingtime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Set;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class LocalDatePaneGenerator {
	private Map<LocalDate, Float> data;

	private String title;

	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.");

	public LocalDatePaneGenerator(Map<LocalDate, Float> data, String title) {
		this.data = data;
		this.title = title;
	}

	public Pane generatePane() {
		VBox rc = new VBox();
		VBox innerBox = new VBox();

		Set<LocalDate> localDates = data.keySet();
		LocalDate minDate = findMinDate(localDates);
		LocalDate maxDate = findMaxDate(localDates);
		generateDatePanes(innerBox, localDates, minDate, maxDate);

		rc.getChildren().add(new Label(title));
		rc.getChildren().add(innerBox);

		return rc;
	}

	private LocalDate findMaxDate(Set<LocalDate> localDates) {
		LocalDate tmpDate = LocalDate.of(0, 1, 1);
		for (LocalDate localDate : localDates) {
			if (tmpDate.isBefore(localDate)) {
				tmpDate = localDate;
			}
		}
		return tmpDate;
	}

	private LocalDate findMinDate(Set<LocalDate> localDates) {
		LocalDate tmpDate = LocalDate.now();
		for (LocalDate localDate : localDates) {
			if (tmpDate.isAfter(localDate)) {
				tmpDate = localDate;
			}
		}
		return tmpDate;
	}

	private void generateDatePanes(VBox innerBox, Set<LocalDate> localDates, LocalDate minDate, LocalDate maxDate) {
		LocalDate currentDate = minDate;

		HBox weekPane = new HBox();

		fillMissingStartingDays(weekPane, minDate);

		for (int i = 0; currentDate.isBefore(maxDate); i++) {
			currentDate = minDate.plusDays(i);

			String dateTitle = currentDate.getDayOfWeek().name().substring(0, 1);
			WorkingTimeEntryPane node;
			String formattedDate = currentDate.format(formatter);
			if (data.containsKey(currentDate)) {
				node = new WorkingTimeEntryPane(data.get(currentDate), dateTitle, formattedDate);
			} else {
				node = new WorkingTimeEntryPane(0, dateTitle, formattedDate);
			}
			weekPane.getChildren().add(node);
			if (currentDate.getDayOfWeek() == DayOfWeek.SUNDAY) {
				innerBox.getChildren().add(weekPane);
				weekPane = new HBox();
			}
		}
		innerBox.getChildren().add(weekPane);
	}

	private void fillMissingStartingDays(HBox weekPane, LocalDate minDate) {
		int nrDays;

		switch (minDate.getDayOfWeek()) {
		case TUESDAY:
			nrDays = 1;
			break;
		case WEDNESDAY:
			nrDays = 2;
			break;
		case THURSDAY:
			nrDays = 3;
			break;
		case FRIDAY:
			nrDays = 4;
			break;
		case SATURDAY:
			nrDays = 5;
			break;
		case SUNDAY:
			nrDays = 6;
			break;
		default:
			nrDays = 0;
			break;
		}

		for (int i = 0; i < nrDays; i++) {
			Pane emptyPane = new Pane();
			emptyPane.setMinHeight(50);
			emptyPane.setMaxHeight(50);
			emptyPane.setPrefHeight(50);

			emptyPane.setMinWidth(50);
			emptyPane.setMaxWidth(50);
			emptyPane.setPrefWidth(50);

			weekPane.getChildren().add(emptyPane);
		}

	}
}
