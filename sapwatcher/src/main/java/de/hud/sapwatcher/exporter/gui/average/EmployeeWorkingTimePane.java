package de.hud.sapwatcher.exporter.gui.average;

import java.io.IOException;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;

public class EmployeeWorkingTimePane extends AnchorPane {

	@FXML
	private StackedBarChart<String, Float> chart;

	@FXML
	private Label title;

	private String titleText;

	public EmployeeWorkingTimePane(String titleText) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("EmployeeWorkingTimeView.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.titleText = titleText;
	}

	public void setData(ObservableList<Series<String, Float>> data) {
		chart.setData(data);
		Series<String, Float> series = chart.getData().get(0);
		Tooltip.install(series.getNode(), new Tooltip("Symbol-0"));
		if (series.getData().size() > 0) {
			Tooltip.install(series.getData().get(0).getNode(), new Tooltip("Symbol-1"));
		}

		title.setText(titleText);
	}

	public void addSeries(Series<String, Float> series) {
		chart.getData().add(series);
	}

}
