package de.hud.sapwatcher.exporter.excel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import de.hud.sapwatcher.data.alm.AlmSapCompareOutputEntry;
import de.hud.sapwatcher.importer.excel.alm.AlmExcelColumnnumberKeeper;

public class AlmSapExcelOutputter {
	private static Logger logger = LogManager.getLogger(AlmSapExcelOutputter.class.getName());

	private XSSFWorkbook wb;

	private Sheet sheet;

	private XSSFCellStyle headerCellStyle;

	private XSSFCellStyle cellGoodStyle;

	private XSSFCellStyle cellNeutralStyle;

	private XSSFCellStyle cellBadStyle;

	private XSSFCellStyle cellDeveloperStyle;

	private void initStyles() {
		headerCellStyle = wb.createCellStyle();
		headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headerCellStyle.setFillPattern(CellStyle.BORDER_THIN);

		cellGoodStyle = wb.createCellStyle();
		cellGoodStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		cellGoodStyle.setFillPattern(CellStyle.BORDER_THIN);
		cellGoodStyle.setDataFormat(wb.createDataFormat().getFormat("##0.00"));

		cellNeutralStyle = wb.createCellStyle();
		cellNeutralStyle.setDataFormat(wb.createDataFormat().getFormat("##0.00"));

		cellBadStyle = wb.createCellStyle();
		cellBadStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		cellBadStyle.setFillPattern(CellStyle.BORDER_THIN);
		cellBadStyle.setDataFormat(wb.createDataFormat().getFormat("##0.00"));
		
		cellDeveloperStyle = wb.createCellStyle();
		cellDeveloperStyle.setWrapText(true);
	}

	private void writeFile(String filename) throws IOException {
		// Write the output to a file
		FileOutputStream out = new FileOutputStream(filename);
		wb.write(out);
		out.close();
	}

	private void createDataRow(Row row, String requirementId, String requirementName, String requirementStatus, float effortPlanned,
			float effortUsed, float effortDiff, String requirementDevelopers) {

		int columnNr = 0;
		Cell cell = row.createCell(columnNr++);
		cell.setCellValue(requirementId);
		cell = row.createCell(columnNr++);
		cell.setCellValue(requirementName);
		cell = row.createCell(columnNr++);
		cell.setCellValue(requirementStatus);

		cell = row.createCell(columnNr++);
		cell.setCellValue(effortPlanned);
		cell.setCellStyle(cellNeutralStyle);

		cell = row.createCell(columnNr++);
		cell.setCellValue(effortUsed);
		cell.setCellStyle(cellNeutralStyle);

		cell = row.createCell(columnNr++);
		cell.setCellValue(effortDiff);
		formatDataCell(cell, effortPlanned, effortDiff);

		cell = row.createCell(columnNr++);
		cell.setCellValue(requirementDevelopers);
		cell.setCellStyle(cellDeveloperStyle);

	}

	private void createHeaderRow(Row titleRow) {
		int columnNr = 0;
		Cell cell = titleRow.createCell(columnNr++);
		formatHeaderCell(cell);
		cell.setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_REQUIREMENT_ID.get(0));

		cell = titleRow.createCell(columnNr++);
		formatHeaderCell(cell);
		cell.setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_REQUIREMENT_NAME.get(0));

		cell = titleRow.createCell(columnNr++);
		formatHeaderCell(cell);
		cell.setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_REQUIREMENT_STATUS.get(0));

		cell = titleRow.createCell(columnNr++);
		formatHeaderCell(cell);
		cell.setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_PLANNED_EFFORT.get(0));

		cell = titleRow.createCell(columnNr++);
		formatHeaderCell(cell);
		cell.setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_USED_EFFORT.get(0));

		cell = titleRow.createCell(columnNr++);
		formatHeaderCell(cell);
		cell.setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_DIFF_EFFORT.get(0));

		cell = titleRow.createCell(columnNr++);
		formatHeaderCell(cell);
		cell.setCellValue(AlmExcelColumnnumberKeeper.TEXT_ALM_DEVELOPERS.get(0));
	}

	private void formatHeaderCell(Cell cell) {
		cell.setCellStyle(headerCellStyle);
	}

	private void formatDataCell(Cell cell, float effortPlanned, float effortDiff) {
		if (Math.abs(effortDiff) < 1) {
			cell.setCellStyle(cellNeutralStyle);
		} else if (effortDiff < 0) {
			cell.setCellStyle(cellGoodStyle);
		} else {
			cell.setCellStyle(cellBadStyle);
		}
	}

	public String createOutput(List<AlmSapCompareOutputEntry> entries) throws IOException {
		logger.entry(entries);

		wb = new XSSFWorkbook();
		sheet = wb.createSheet("Sap-Alm-Compare");
		initStyles();

		int cntRows = 0;
		// title row
		Row titleRow = sheet.createRow(cntRows++);
		createHeaderRow(titleRow);

		for (AlmSapCompareOutputEntry entry : entries) {
			String requirementDevelopers = extractDevelopers(entry);

			createDataRow(sheet.createRow(cntRows), entry.getRequirementId(), entry.getRequirementName(), entry.getRequirementStatus(),
					entry.getEffortPlanned(), entry.getEffortCurrentTotal(), entry.getEffortDifference(), requirementDevelopers);
			sheet.autoSizeColumn(cntRows);

			cntRows++;
		}
		String filename = System.getProperty("user.home") + "/Desktop/Sap-Alm_" + System.currentTimeMillis() + ".xlsx";
		writeFile(filename);

		return logger.exit(filename);
	}

	private String extractDevelopers(AlmSapCompareOutputEntry entry) {
		StringBuffer rc = new StringBuffer();

		for (String developer : entry.getEffortCurrentDetails().keySet()) {
			rc.append(developer);
			rc.append(" (");
			rc.append(String.format("%4.2f", entry.getEffortCurrentDetails().get(developer)));
			rc.append(")");

			rc.append("\n");
		}

		return rc.toString().trim();
	}
}
