package de.hud.sapwatcher.exporter.excel;

import de.hud.sapwatcher.data.OutputDataEntry;
import de.hud.sapwatcher.data.OutputDataStructure;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.List;
import java.util.Map;

public class SapExcelOutputterImpl {

	private boolean outputPrices = true;
	private int cntRows = 0;
	private XSSFCellStyle headerCellStyle;
	private XSSFCellStyle contentCellStyle;

	private final int COLUMN_MAIN_NAME = 0;
	private final int COLUMN_SUB_NAME = 1;
	private final int COLUMN_HOURS = 2;
	private final int COLUMN_PRICE = 3;

	public XSSFWorkbook toWorkbook(List<OutputDataStructure> outputStructures) {
		XSSFWorkbook wb = new XSSFWorkbook();

		initStyles(wb);

		for (OutputDataStructure outputDataStructure : outputStructures) {
			cntRows = 0;
			XSSFSheet sheet = wb.createSheet(outputDataStructure.getSectionName());
			addHeader(sheet
					, outputDataStructure.getSectionName()
					, outputDataStructure.getTotalSumHours()
					, outputDataStructure.getTotalSumPrice()
					, outputDataStructure.getBudget());
			//add empty line
			cntRows++;

			addEntriesToOutput(sheet, outputDataStructure.getSubEntries());

			setColumnWidths(sheet);
		}

		return wb;
	}
	private void initStyles(XSSFWorkbook wb) {
		headerCellStyle = wb.createCellStyle();
		headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headerCellStyle.setFillPattern(CellStyle.BORDER_THIN);

		contentCellStyle = wb.createCellStyle();
		contentCellStyle.setFillForegroundColor(IndexedColors.BLACK.getIndex());
		contentCellStyle.setFillPattern(CellStyle.BORDER_NONE);
	}

	private void addEntriesToOutput(XSSFSheet sheet, Map<String, OutputDataEntry> entrys) {
		for (String key : entrys.keySet()) {
			Row titleRow = sheet.createRow(cntRows++);

			OutputDataEntry entry = entrys.get(key);
			String string = convertToReadableString(key);

			Cell cell = titleRow.createCell(COLUMN_MAIN_NAME);
			formatContentCell(cell);
			addCellSpanHorizontal(sheet, cntRows, COLUMN_MAIN_NAME);
			cell.setCellValue(string);

			cell = titleRow.createCell(COLUMN_HOURS);
			formatContentCell(cell);
			cell.setCellValue(entry.getHours());

			addPrice(sheet, entry);

			outputSubEntries(sheet, entry);
		}
	}

	private void outputSubEntries(XSSFSheet sheet, OutputDataEntry entry) {
		Map<String, OutputDataEntry> subEntries = entry.getSubEntries();
		for (String subKey : subEntries.keySet()) {
			OutputDataEntry subEntry = entry.getSubEntries().get(subKey);
			String entryName = convertToReadableString(subKey);

			Row row = sheet.createRow(cntRows++);

			Cell cell = row.createCell(COLUMN_SUB_NAME);
			formatContentCell(cell);
			cell.setCellValue(entryName);

			cell = row.createCell(COLUMN_HOURS);
			formatContentCell(cell);
			cell.setCellValue(subEntry.getHours());

			addPrice(sheet, subEntry);
		}
		if (!subEntries.keySet().isEmpty()) {
			cntRows++;
		}
	}

	private void addPrice(XSSFSheet sheet, OutputDataEntry entry) {
		if (outputPrices && (null != entry.getSoldService()) && (!entry.getSoldService().equalsIgnoreCase("<LEER>"))) {
			Row row = sheet.createRow(cntRows++);

			Cell cell = row.createCell(COLUMN_MAIN_NAME);
			formatContentCell(cell);
			addCellSpanHorizontal(sheet, cntRows, COLUMN_MAIN_NAME);
			cell.setCellValue(entry.getSoldService());

			if ((null != entry.getSumPrices()) && (0 != entry.getSumPrices())) {
				float price = entry.getSumPrices();

				cell = row.createCell(COLUMN_PRICE);
				formatContentCell(cell);
				cell.setCellValue(String.format("%10.2f€", price));
			} else {
				cell = row.createCell(COLUMN_PRICE);
				formatContentCell(cell);
				cell.setCellValue("nicht bekannt");
			}
		}
	}

	private void formatHeaderCell(Cell cell) {
		cell.setCellStyle(headerCellStyle);
	}

	private void formatContentCell(Cell cell) {
		cell.setCellStyle(contentCellStyle);
	}

	private void addHeader(XSSFSheet sheet, String title, float time, float price, float budget) {
		Row titleRow = sheet.createRow(cntRows++);

		Cell cell = titleRow.createCell(COLUMN_MAIN_NAME);
		formatHeaderCell(cell);
		addCellSpanHorizontal(sheet, cntRows, COLUMN_MAIN_NAME);
		cell.setCellValue(title);

		cell = titleRow.createCell(COLUMN_HOURS);
		formatHeaderCell(cell);
		cell.setCellValue(time);

		if (outputPrices && (budget != 0)) {
			cell = titleRow.createCell(COLUMN_PRICE);
			formatHeaderCell(cell);
			cell.setCellValue("Verfügbar:"+budget);

			cell = titleRow.createCell(COLUMN_PRICE + 1);
			formatHeaderCell(cell);
			cell.setCellValue("Verbraucht:"+price);

			cell = titleRow.createCell(COLUMN_PRICE + 2);
			formatHeaderCell(cell);
			cell.setCellValue("Rest:"+ (budget - price));

		}
	}

	private void addCellSpanHorizontal(XSSFSheet sheet, int row, int column) {
		sheet.addMergedRegion(new CellRangeAddress(
				row - 1, //first row (0-based)
				row - 1, //last row  (0-based)
				column, //first column (0-based)
				column + 1  //last column  (0-based)
		));
	}

	private String convertToReadableString(String text) {
		if (text.trim().isEmpty()) {
			return "<LEER>";
		}
		return text;
	}

	public void setColumnWidths(XSSFSheet sheet) {
//		sheet.setColumnWidth(COLUMN_MAIN_NAME, 200);
//		sheet.setColumnWidth(COLUMN_HOURS, 100);
//		sheet.setColumnWidth(COLUMN_SUB_NAME, 100);
//		sheet.setColumnWidth(COLUMN_PRICE, 100);
//		sheet.setColumnWidth(COLUMN_PRICE + 1, 100);
//		sheet.setColumnWidth(COLUMN_PRICE + 2, 100);
		sheet.autoSizeColumn(COLUMN_MAIN_NAME);
		sheet.autoSizeColumn(COLUMN_HOURS);
		sheet.autoSizeColumn(COLUMN_SUB_NAME);
		sheet.autoSizeColumn(COLUMN_PRICE);
		sheet.autoSizeColumn(COLUMN_PRICE + 1);
		sheet.autoSizeColumn(COLUMN_PRICE + 2);
	}
}
