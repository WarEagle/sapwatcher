package de.hud.sapwatcher.exporter.gui.workingtime;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

class WorkingTimeEntryPane extends StackPane {

	@FXML
	private Label hours;

	@FXML
	private Label day;

	@FXML
	private Label title;

	private final String BACKGROUND = "background-light";

	private final String BACKGROUND_RED = "background-light-red";

	private final String BACKGROUND_DARK_RED = "background-dark-red";

	private final String BACKGROUND_GREEN = "background-light-green";

	private final String BACKGROUND_DARK_GREEN = "background-dark-green";

	private void setCorrectLayout(float hours) {
		this.getStyleClass().remove(BACKGROUND);
		this.getStyleClass().remove(BACKGROUND_RED);
		this.getStyleClass().remove(BACKGROUND_GREEN);

		if (hours < 4f) {
			this.getStyleClass().add(BACKGROUND_DARK_RED);
		} else if (hours < 7.75f) {
			this.getStyleClass().add(BACKGROUND_RED);
		} else if (hours >= 9.5f) {
			this.getStyleClass().add(BACKGROUND_DARK_GREEN);
		} else if (hours > 8.25f) {
			this.getStyleClass().add(BACKGROUND_GREEN);
		} else {
			this.getStyleClass().add(BACKGROUND);
		}
	}

	public WorkingTimeEntryPane(float hours, String description, String title) {
		init();
		this.hours.setText(String.format("%.2f", hours));
		this.day.setText(description);
		this.title.setText(title);
		setCorrectLayout(hours);
	}

	public WorkingTimeEntryPane(float hours, String description) {
		init();
		this.hours.setText(String.format("%.2f", hours));
		this.day.setText(description);
		this.title.setVisible(false);
		setCorrectLayout(hours);
	}

	public WorkingTimeEntryPane(float hours) {
		init();
		this.hours.setText(String.format("%.2f", hours));
		this.hours.setVisible(false);
		this.title.setVisible(false);
		setCorrectLayout(hours);
	}

	private void init() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("WorkingTimeEntryView.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}
}
