package de.hud.sapwatcher.exporter.ascii;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import de.hud.sapwatcher.data.OutputDataEntry;
import de.hud.sapwatcher.data.OutputDataStructure;
import de.hud.sapwatcher.data.WorkingTimeEntry;

public class AsciiOutputterImpl implements AsciiOutputter {

	private boolean outputPrices = true;

	private final int LINE_WIDTH = 50;

	@Override
	public String toReadableText(List<OutputDataStructure> outputStructures) {
		StringBuffer rc = new StringBuffer();

		for (OutputDataStructure outputDataStructure : outputStructures) {
			addHeader(rc //
					, outputDataStructure.getSectionName() //
					, outputDataStructure.getTotalSumHours() //
					, outputDataStructure.getTotalSumPrice() //
					, outputDataStructure.getBudget());
			addEntriesToOutput(rc, outputDataStructure.getSubEntries());
			rc.append("\n");
		}

		return rc.toString();
	}

	private void addEntriesToOutput(StringBuffer text, Map<String, OutputDataEntry> entrys) {
		for (String key : entrys.keySet()) {
			OutputDataEntry entry = entrys.get(key);
			String string = convertToReadableString(key);
			text.append(String.format(" %-" + (LINE_WIDTH - 1) + "s %8.2f", string.substring(0, Math.min(39, string.length())), entry.getHours()));
			addPrice(text, entry);
			text.append("\n");

			outputSubEntries(text, entry);
		}
	}

	private void outputSubEntries(StringBuffer text, OutputDataEntry entry) {
		Map<String, OutputDataEntry> subEntries = entry.getSubEntries();
		for (String subKey : subEntries.keySet()) {
			OutputDataEntry subEntry = entry.getSubEntries().get(subKey);
			String string = convertToReadableString(subKey);
			text.append(String.format("   %-" + (LINE_WIDTH - 3) + "s %8.2f", string.substring(0, Math.min(37, string.length())), subEntry.getHours()));
			addPrice(text, subEntry);
			text.append("\n");
		}
		if (!subEntries.keySet().isEmpty()) {
			text.append("\n");
		}
	}

	private void addPrice(StringBuffer text, OutputDataEntry entry) {
		if (outputPrices && (null != entry.getSoldService()) && (!entry.getSoldService().equalsIgnoreCase("<LEER>"))) {
			text.append(" ");
			if ((null != entry.getSumPrices()) && (0 != entry.getSumPrices())) {
				float price = entry.getSumPrices();
				text.append(String.format("(%-6s = %10.2f€)", entry.getSoldService(), price));
			} else {
				text.append(String.format("(%s nicht bekannt.)", entry.getSoldService()));
			}
		}
	}

	private void addHeader(StringBuffer text, String title, float time, float price, float budget) {
		addDivider(text, LINE_WIDTH + 9);
		text.append(String.format("| %-" + (LINE_WIDTH - 6) + "s %11.2fh |", title, time));
		if (outputPrices && (budget != 0)) {
			text.append("\n");
			text.append(String.format("| %" + (LINE_WIDTH - 6) + "s %11.2f€ |", "Verfügbar: ", budget));
			text.append("\n");
			text.append(String.format("| %" + (LINE_WIDTH - 6) + "s %11.2f€ |", "Verbraucht:", price));
			text.append("\n");
			text.append(String.format("| %" + (LINE_WIDTH - 6) + "s %11.2f€ |", "Rest:", budget - price));
		}
		text.append("\n");
		addDivider(text, LINE_WIDTH + 9);

	}

	private void addDivider(StringBuffer rc, int widthHeader) {
		rc.append("+");
		for (int i = 0; i < widthHeader; i++) {
			rc.append("-");
		}
		rc.append("+");
		rc.append("\n");
	}

	private String convertToReadableString(String text) {
		if (text.trim().isEmpty()) {
			return "<LEER>";
		}
		return text;
	}

	@Override
	public String toReadableEmailText(List<WorkingTimeEntry> workingEntries) {
		StringBuffer rc = new StringBuffer();

		rc.append(String.format("%-20s", "Name"));
		rc.append(" | ");
		rc.append(String.format("%-14s", "Datum"));
		rc.append(" | ");
		rc.append(String.format("%-15s", "Task"));
		rc.append(" | ");
		rc.append(String.format("%-20s", "Beschreibung"));
		rc.append("\n");
		rc.append("---------------------+----------------+-----------------+---------------------------------------");
		rc.append("\n");

		TreeSet<String> lines = new TreeSet<>();
		for (WorkingTimeEntry workingTimeEntry : workingEntries) {
			DateTimeFormatter formatterDay = DateTimeFormatter.ofPattern("dd.MM.uu HH:mm");

			StringBuffer line = new StringBuffer();
			line.append(String.format("%-20s", workingTimeEntry.getEmployee()));
			line.append(" | ");
			line.append(String.format("%-14s", workingTimeEntry.getStartDate().format(formatterDay)));
			line.append(" | ");
			line.append(String.format("%-15s", workingTimeEntry.getTask()));
			line.append(" | ");
			line.append(String.format("%-20s", workingTimeEntry.getDescription()));

			
			lines.add(line.toString());
		}
		
		for (String line : lines) {
			rc.append(line);
			rc.append("\n");
		}

		return rc.toString();
	}
}
