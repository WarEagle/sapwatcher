package de.hud.sapwatcher.exporter.ascii;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.alm.AlmSapCompareOutputEntry;
import de.hud.sapwatcher.helper.StringHelper;

public class AlmSapAsciiOutputter {
	private static Logger logger = LogManager.getLogger(AlmSapAsciiOutputter.class.getName());

	public String createOutput(List<AlmSapCompareOutputEntry> entries) {
		logger.entry(entries);

		StringBuffer rc = new StringBuffer();

		rc.append(String.format("%-10s | %-36s | %-6s | %-6s | %-7s |", "Requirement", "Beschreibung", "Soll", "Ist", "Diff"));
		rc.append("\n");
		rc.append("------------+--------------------------------------+--------+--------+---------+");
		rc.append("\n");

		for (AlmSapCompareOutputEntry entry : entries) {
			rc.append(String.format("%-11s | %-36s | %6.2f | %6.2f | %7.2f |", entry.getRequirementId(),
					StringHelper.shortenTo(entry.getRequirementName(), 36), entry.getEffortPlanned(), entry.getEffortCurrentTotal(),
					entry.getEffortDifference()));
			rc.append("\n");
		}

		return logger.exit(rc.toString());
	}
}
