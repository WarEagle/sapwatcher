package de.hud.sapwatcher.exporter.gui.workingtime;

import java.time.DayOfWeek;
import java.util.Map;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class DayOfMonthPaneGenerator {
	private Map<DayOfWeek, Float> data;

	private String title;

	public DayOfMonthPaneGenerator(Map<DayOfWeek, Float> data, String title) {
		this.data = data;
		this.title = title;
		
	}

	public Pane generatePane() {
		VBox rc = new VBox();

		rc.getChildren().add(new Label(title));

		HBox innerBox = new HBox();

		for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
			WorkingTimeEntryPane node;
			if (data.containsKey(dayOfWeek)) {
				node = new WorkingTimeEntryPane(data.get(dayOfWeek), dayOfWeek.name().substring(0, 1));
			} else {
				node = new WorkingTimeEntryPane(0, dayOfWeek.name().substring(0, 1));
			}
			innerBox.getChildren().add(node);
		}

		rc.getChildren().add(innerBox);

		return rc;
	}
}
