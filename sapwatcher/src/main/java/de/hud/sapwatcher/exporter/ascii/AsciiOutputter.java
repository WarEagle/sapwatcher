package de.hud.sapwatcher.exporter.ascii;

import java.util.List;

import de.hud.sapwatcher.data.OutputDataStructure;
import de.hud.sapwatcher.data.WorkingTimeEntry;

public interface AsciiOutputter {
	
	String toReadableText(List<OutputDataStructure> outputs);
	String toReadableEmailText(List<WorkingTimeEntry> workingEntries);

}