package de.hud.sapwatcher.exception;

public class InvalidImportFileException extends Exception {

	private String message;

	private static final long serialVersionUID = -6294045913293179833L;

	public InvalidImportFileException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
