package de.hud.sapwatcher;

import java.io.File;
import java.nio.file.Paths;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.Config;

public class Starter extends Application {

	private static Logger logger = LogManager.getLogger(Starter.class.getName());
	
	private Config config = new Config();

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		logger.entry(primaryStage);

		config.readFromFile();
		
		Image icon = new Image(ApplicationController.class.getResource("/de/hud/sapwatcher/gui/pics/icons/appicon.png").toExternalForm());
		primaryStage.getIcons().add(icon);
		primaryStage.setTitle("SAP Watcher 0.4 developer version");

		primaryStage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });

		Platform.setImplicitExit(false);

		ApplicationController applicationController = new ApplicationController();
		applicationController.init();
		applicationController.displayMainApplication(primaryStage);
		applicationController.setConfig(config);

		logger.debug("param.size=" + getParameters().getRaw().size());

		if (getParameters().getRaw().size() == 1) {
			applicationController.loadFile(Paths.get(new File(getParameters().getRaw().get(0)).toURI()));
			applicationController.fillContentsOnGUI();
		}
	}
}
