package de.hud.sapwatcher.importer.excel.alm;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;

import de.hud.sapwatcher.exception.InvalidImportFileException;
import de.hud.sapwatcher.importer.excel.AbstractExcelColumnnumberKeeper;

public class AlmExcelColumnnumberKeeper extends AbstractExcelColumnnumberKeeper {
	public static final List<String> TEXT_ALM_PLANNED_EFFORT = Arrays.asList("Planned Effort", "PlannedEffort");

	public static final List<String> TEXT_ALM_EFFORT = Collections.singletonList("Effort");

	public static final List<String> TEXT_ALM_USED_EFFORT = Collections.singletonList("Used Effort");

	public static final List<String> TEXT_ALM_DIFF_EFFORT = Collections.singletonList("Diff Effort");

	public static final List<String> TEXT_ALM_REQUIREMENT_ID = Collections.singletonList("ReqID");

	public static final List<String> TEXT_ALM_REQUIREMENT_NAME = Arrays.asList("Name", "ReqName");

	public static final List<String> TEXT_ALM_REQUIREMENT_STATUS = Arrays.asList("Status", "ReqStatus");

	public static final List<String> TEXT_ALM_DEVELOPERS = Collections.singletonList("Entwickler");

	public static final List<String> TEXT_ALM_DEFECT_ID = Arrays.asList("Defect ID", "DefID");

	public static final List<String> TEXT_ALM_DEFECT_STATUS = Arrays.asList("Defect Status", "DefStatus");

	private int COLUMN_NR_PLANNED_EFFORT;

	private int COLUMN_NR_EFFORT;

	private int COLUMN_NR_REQUIREMENT_ID;

	private int COLUMN_NR_REQUIREMENT_NAME;

	private int COLUMN_NR_REQUIREMENT_STATUS;

	private int COLUMN_NR_ALM_DEFECT_ID;

	private int COLUMN_NR_ALM_DEFECT_STATUS;

	public int getColumnNrEffort() {
		return COLUMN_NR_EFFORT;
	}

	public int getColumnNrRequirementId() {
		return COLUMN_NR_REQUIREMENT_ID;
	}

	public int getColumnNrRequirementName() {
		return COLUMN_NR_REQUIREMENT_NAME;
	}

	public int getColumnNrRequirementStatus() {
		return COLUMN_NR_REQUIREMENT_STATUS;
	}

	public int getColumnNrDefectId() {
		return COLUMN_NR_ALM_DEFECT_ID;
	}

	public int getColumnNrDefectStatus() {
		return COLUMN_NR_ALM_DEFECT_STATUS;
	}

	@Override
	protected void resetValues() {
		COLUMN_NR_PLANNED_EFFORT = DEFAULT_NOT_FOUND;
		COLUMN_NR_EFFORT = DEFAULT_NOT_FOUND;
		COLUMN_NR_REQUIREMENT_ID = DEFAULT_NOT_FOUND;
		COLUMN_NR_REQUIREMENT_NAME = DEFAULT_NOT_FOUND;
		COLUMN_NR_REQUIREMENT_STATUS = DEFAULT_NOT_FOUND;
		COLUMN_NR_ALM_DEFECT_ID = DEFAULT_NOT_FOUND;
		COLUMN_NR_ALM_DEFECT_STATUS = DEFAULT_NOT_FOUND;
	}

	@Override
	protected void checkAllValuesFilledOrThrowException() throws InvalidImportFileException {
		if (COLUMN_NR_PLANNED_EFFORT == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_ALM_PLANNED_EFFORT + " not found.");
		} else if (COLUMN_NR_REQUIREMENT_ID == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_ALM_REQUIREMENT_ID + " not found.");
		} else if (COLUMN_NR_EFFORT == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_ALM_EFFORT + " not found.");
		} else if (COLUMN_NR_REQUIREMENT_NAME == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_ALM_REQUIREMENT_NAME + " not found.");
		} else if (COLUMN_NR_REQUIREMENT_STATUS == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_ALM_REQUIREMENT_STATUS + " not found.");
		} else if (COLUMN_NR_ALM_DEFECT_ID == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_ALM_DEFECT_ID + " not found.");
		} else if (COLUMN_NR_ALM_DEFECT_STATUS == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_ALM_DEFECT_STATUS + " not found.");
		}
	}

	@Override
	protected void checkCellMatchingTopic(short columnNumber, Cell cell) {

		if (matches(cell.getStringCellValue(), TEXT_ALM_PLANNED_EFFORT)) {
			COLUMN_NR_PLANNED_EFFORT = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_ALM_REQUIREMENT_ID)) {
			COLUMN_NR_REQUIREMENT_ID = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_ALM_EFFORT)) {
			COLUMN_NR_EFFORT = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_ALM_REQUIREMENT_NAME)) {
			COLUMN_NR_REQUIREMENT_NAME = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_ALM_REQUIREMENT_STATUS)) {
			COLUMN_NR_REQUIREMENT_STATUS = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_ALM_DEFECT_ID)) {
			COLUMN_NR_ALM_DEFECT_ID = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_ALM_DEFECT_STATUS)) {
			COLUMN_NR_ALM_DEFECT_STATUS = columnNumber;
		}
	}

}
