package de.hud.sapwatcher.importer.excel.sap;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import de.hud.sapwatcher.importer.excel.AbstractExcelColumnnumberKeeper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.POIXMLDocumentPart;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRelation;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTTwoCellAnchor;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.WsDrDocument;

import de.hud.sapwatcher.data.WorkingTimeEntry;
import de.hud.sapwatcher.exception.InvalidImportFileException;
import de.hud.sapwatcher.helper.StringHelper;

public class SapExcelImporter {

	private static Logger logger = LogManager.getLogger(SapExcelImporter.class.getName());

	private SapExcelColumnnumberKeeper columnnumberKeeper = new SapExcelColumnnumberKeeper();

	private Set<Integer> deniedEntriesColumnRow;

	private DataFormatter formatter = new DataFormatter(Locale.GERMAN);

	public List<WorkingTimeEntry> readFile(Path filename) throws InvalidImportFileException {
		List<WorkingTimeEntry> rc = new ArrayList<>();

		try {
			//Prevent Zip-Bomb-Exception
			ZipSecureFile.setMinInflateRatio(0.00001);
			ZipSecureFile.setMaxEntrySize(4294967295L);
			Workbook wb = WorkbookFactory.create(filename.toFile());

			Sheet sheet = wb.getSheetAt(0);

			try {
				deniedEntriesColumnRow = getDrawings(sheet);
			} catch (XmlException e) {
				e.printStackTrace();
				logger.error(e);
				System.exit(-1);
			}

			columnnumberKeeper.createColumnsBasedOnHeader(sheet.getRow(sheet.getFirstRowNum()));

			sheet.forEach((row) -> {
				WorkingTimeEntry entry = processRow(row);
				if (entry != null) {
					rc.add(entry);
					logger.debug("created " + entry);
				}
			});

		} catch (InvalidFormatException | IOException e) {
			logger.error("Cannot read '" + filename + "'", e);
			System.exit(-1);
		}

		return logger.exit(rc);
	}

	private Set<Integer> getDrawings(Sheet mySheet) throws InvalidFormatException, XmlException, IOException {
		Set<Integer> deniedEntriesColumnRow = new HashSet<>();

		XSSFSheet sheet = (XSSFSheet) mySheet;
		for (POIXMLDocumentPart pdp : sheet.getRelations()) {
			if (!XSSFRelation.DRAWINGS.getRelation().equals(pdp.getPackageRelationship().getRelationshipType()))
				continue;

			PackagePart drawPP = pdp.getPackagePart();
			WsDrDocument draw = WsDrDocument.Factory.parse(drawPP.getInputStream());

			for (CTTwoCellAnchor twoAnc : draw.getWsDr().getTwoCellAnchorList()) {

				String name = twoAnc.getPic().getNvPicPr().getCNvPr().getName();
				if (name.contains("Storniert") //
						|| name.contains("Cancelled")//
						|| name.contains("Nach Genehmigung geändert")//
				) {
					deniedEntriesColumnRow.add(twoAnc.getFrom().getRow());
				}
			}
		}
		return deniedEntriesColumnRow;

	}

	private boolean isValidRow(DataFormatter formatter, Row row) {
		boolean rc = Boolean.FALSE;

		try {
			/* just to see, if we get an exception */
			row.getCell(columnnumberKeeper.getRowNumberDate()).getDateCellValue();

			String cellcontent = formatter.formatCellValue(row.getCell(columnnumberKeeper.getRowNumberDate()));

			if ((cellcontent == null) || (cellcontent.trim().length() == 0)) {
				// ignore rows which contain no employeename
				return rc;
			} else if (row.getRowNum() == 0) {
				// ignore first row, because it contains the header
				return rc;
			}

			if (deniedEntriesColumnRow.contains(row.getRowNum())) {
				return rc;
			}

			rc = true;
		} catch (IllegalStateException e) {
			// use this to generate false-rc
		}

		return rc;
	}

	private WorkingTimeEntry processRow(Row row) {
		logger.entry(row, formatter);

		WorkingTimeEntry rc = null;
		try {

			if (!isValidRow(formatter, row)) {
				return logger.exit(rc);
			}

			String employee = formatter.formatCellValue(row.getCell(columnnumberKeeper.getRowNumberEmpoyee()));
			String description = formatter.formatCellValue(row.getCell(columnnumberKeeper.getRowNumberDescription()));
			Date date = row.getCell(columnnumberKeeper.getRowNumberDate()).getDateCellValue();
			String project = formatter.formatCellValue(row.getCell(columnnumberKeeper.getRowNumberProject()));
			String task = formatter.formatCellValue(row.getCell(columnnumberKeeper.getRowNumberTask()));
			String soldservice = "???";
            if (columnnumberKeeper.getRowNumberSoldService() != AbstractExcelColumnnumberKeeper.DEFAULT_NOT_FOUND){
                soldservice = formatter.formatCellValue(row.getCell(columnnumberKeeper.getRowNumberSoldService()));
            }

            LocalDateTime startTime = findStartTime(row, date);
            LocalDateTime endTime = findEndTime(row, date);

            rc = new WorkingTimeEntry(employee,
					startTime,
					endTime,
					project,
					task,
					description,
					soldservice);
		} catch (IllegalStateException e) {
			logger.error("row.nr=" + row.getRowNum() + ",row=" + row, e);
		}

		return logger.exit(rc);
	}

    private LocalDateTime findStartTime(Row row, Date date) {
        if (columnnumberKeeper.getRowNumberStartTime() != AbstractExcelColumnnumberKeeper.DEFAULT_NOT_FOUND){
            Date starttime = row.getCell(columnnumberKeeper.getRowNumberStartTime()).getDateCellValue();
            return StringHelper.createTimeFromPartialStrings(date, starttime);
        }
        LocalDateTime dateTime = LocalDateTime.of(date.getYear()+1900, date.getMonth()+1, date.getDay(), 0, 0);
        return dateTime;

    }

    private LocalDateTime findEndTime(Row row, Date date) {
        if (columnnumberKeeper.getRowNumberEndTime() != AbstractExcelColumnnumberKeeper.DEFAULT_NOT_FOUND) {
            Date endTime = row.getCell(columnnumberKeeper.getRowNumberEndTime()).getDateCellValue();
            return StringHelper.createTimeFromPartialStrings(date, endTime);
        }

        Double cntUnits = row.getCell(columnnumberKeeper.getRowCntUnits()).getNumericCellValue();
        LocalDateTime dateTime = LocalDateTime.of(date.getYear()+1900, date.getMonth()+1, date.getDay(), getHours(cntUnits), getMinutes(cntUnits));
        return dateTime;

    }

    protected int getMinutes(Double hours){
        return (int) ((hours - hours.intValue()) * 60);
    }

    protected int getHours(Double hours){
        return hours.intValue();
    }

}
