package de.hud.sapwatcher.importer.excel.alm;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import de.hud.sapwatcher.data.alm.AlmSapCompareEntry;
import de.hud.sapwatcher.exception.InvalidImportFileException;

public class AlmExcelImporter {
	private static Logger logger = LogManager.getLogger(AlmExcelImporter.class.getName());

	private AlmExcelColumnnumberKeeper columnnumberKeeper = new AlmExcelColumnnumberKeeper();

	private DataFormatter formatter = new DataFormatter(Locale.GERMAN);

	public List<AlmSapCompareEntry> readFile(Path filename) throws InvalidImportFileException {
		List<AlmSapCompareEntry> rc = new ArrayList<>();

		try {
			Workbook wb = WorkbookFactory.create(filename.toFile());

			Sheet sheet = wb.getSheetAt(0);

			columnnumberKeeper.createColumnsBasedOnHeader(sheet.getRow(sheet.getFirstRowNum()));

			sheet.forEach((row) -> {
				AlmSapCompareEntry entry = processRow(row);
				if (entry != null) {
					rc.add(entry);
					logger.debug("created " + entry);
				}
			});

		} catch (InvalidFormatException | IOException e) {
			logger.error("Cannot read '" + filename + "'", e);
			System.exit(-1);
		}

		return logger.exit(rc);
	}

	private boolean isValidRow(Row row) {
		boolean rc = Boolean.FALSE;

		try {
			if (row.getRowNum() == 0) {
				// ignore first row, because it contains the header
				return rc;
			}

			rc = true;
		} catch (IllegalStateException | NullPointerException e) {
			// use this to generate false-rc
		}

		return rc;
	}

	private AlmSapCompareEntry processRow(Row row) {
		logger.entry(row, formatter);

		AlmSapCompareEntry rc = null;
		try {

			if (!isValidRow(row)) {
				return logger.exit(rc);
			}

			String effortAsString = formatter.formatCellValue(row.getCell(columnnumberKeeper.getColumnNrEffort()));
			String requirementId = formatter.formatCellValue(row.getCell(columnnumberKeeper.getColumnNrRequirementId()));
			String requirementStatus = formatter.formatCellValue(row.getCell(columnnumberKeeper.getColumnNrRequirementStatus()));
			String requirementName = formatter.formatCellValue(row.getCell(columnnumberKeeper.getColumnNrRequirementName()));
			String defectId = formatter.formatCellValue(row.getCell(columnnumberKeeper.getColumnNrDefectId()));
			String defectStatus= formatter.formatCellValue(row.getCell(columnnumberKeeper.getColumnNrDefectStatus()));

			if ((requirementId == null) || (requirementId.isEmpty())) {
				return null;
			}

			rc = new AlmSapCompareEntry();
			// * 8 because we are using hours and alm uses days
			rc.setEffort(getAsNumber(effortAsString) * 8);
			rc.setRequirementId(requirementId);
			rc.setRequirementStatus(requirementStatus);
			rc.setRequirementName(requirementName);
			rc.setDefectId(defectId);
			rc.setDefectStatus(defectStatus);



		} catch (NumberFormatException | IllegalStateException e) {
			logger.error("Error while reading row. Ignoring row.nr=" + row.getRowNum() + ",row=" + row, e);
		}

		return logger.exit(rc);
	}

	private float getAsNumber(String plannedEffortAsString) {
		float rc;
		try {
			rc = Float.parseFloat(plannedEffortAsString);
		} catch (NumberFormatException e) {
			rc = 0f;
		}
		return rc;
	}

}
