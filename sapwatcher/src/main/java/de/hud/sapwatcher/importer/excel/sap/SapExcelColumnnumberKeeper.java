package de.hud.sapwatcher.importer.excel.sap;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;

import de.hud.sapwatcher.exception.InvalidImportFileException;
import de.hud.sapwatcher.importer.excel.AbstractExcelColumnnumberKeeper;

public class SapExcelColumnnumberKeeper extends AbstractExcelColumnnumberKeeper {
	static final List<String> TEXT_CORRECTIONINDICATOR = Arrays.asList("Correction Indicator", "Korrekturkennzeichen",
			"Status-/Prozessierungskennzeichen");

	public static final List<String> TEXT_STARTTIME = Arrays.asList("Beginnuhrzeit", "Start time");

	public static final List<String> TEXT_ENDTIME = Arrays.asList("Endeuhrzeit", "End time");

	public static final List<String> TEXT_CNT_UNITS = Collections.singletonList("Anzahl (Maßeinheit)");

	public static final List<String> TEXT_DATE = Arrays.asList("Datum", "Date");

	public static final List<String> TEXT_SOLDSERVICE = Arrays.asList("verkaufte Leistung", "Material");

	public static final List<String> TEXT_EMPLOYEE = Arrays.asList("Name des Mitarbeiters bzw. Bewerbers", "Name of Employee or Applicant");

	public static final List<String> TEXT_PROJECT = Arrays.asList("Allgemeiner Empfänger", "General receiver", "Empfänger-PSP-Element");

	public static final List<String> TEXT_TASK = Arrays.asList("Tätigkeit", "Kontierungstext");

	public static final List<String> TEXT_DESCRIPTION = Arrays.asList("Kurztext", "Short Text");

	public static final List<String> TEXT_PERSONALID = Arrays.asList("Personalnummer", "Personnel Number");

	private int COLUMNNR_CORRECTIONINDICATOR;

	private int COLUMNNR_EMPLOYEE;

	private int COLUMNNR_DESCRIPTION;

	private int COLUMNNR_DATE;

	private int COLUMNNR_STARTTIME;

	private int COLUMNNR_ENDTIME;

	private int COLUMNNR_SOLDSERVICE;

	private int COLUMNNR_PROJECT;

	private int COLUMNNR_TASK;

	private int COLUMNNR_CNT_UNITS;

	public int getRowNumberCorrectionIndicator() {
		return COLUMNNR_CORRECTIONINDICATOR;
	}

	public int getRowNumberEmpoyee() {
		return COLUMNNR_EMPLOYEE;
	}

	public int getRowNumberDescription() {
		return COLUMNNR_DESCRIPTION;
	}

	public int getRowNumberDate() {
		return COLUMNNR_DATE;
	}

	public int getRowNumberStartTime() {
		return COLUMNNR_STARTTIME;
	}

	public int getRowNumberEndTime() {
		return COLUMNNR_ENDTIME;
	}

	public int getRowNumberSoldService() {
		return COLUMNNR_SOLDSERVICE;
	}

	public int getRowNumberProject() {
		return COLUMNNR_PROJECT;
	}

	public int getRowNumberTask() {
		return COLUMNNR_TASK;
	}

	public int getRowCntUnits() {
		return COLUMNNR_CNT_UNITS;
	}

	@Override
	protected void resetValues() {
		COLUMNNR_CORRECTIONINDICATOR = DEFAULT_NOT_FOUND;
		COLUMNNR_EMPLOYEE = DEFAULT_NOT_FOUND;
		COLUMNNR_DESCRIPTION = DEFAULT_NOT_FOUND;
		COLUMNNR_DATE = DEFAULT_NOT_FOUND;
		COLUMNNR_STARTTIME = DEFAULT_NOT_FOUND;
		COLUMNNR_ENDTIME = DEFAULT_NOT_FOUND;
		COLUMNNR_SOLDSERVICE = DEFAULT_NOT_FOUND;
		COLUMNNR_PROJECT = DEFAULT_NOT_FOUND;
		COLUMNNR_TASK = DEFAULT_NOT_FOUND;
		COLUMNNR_CNT_UNITS = DEFAULT_NOT_FOUND;
	}

	@Override
	protected void checkAllValuesFilledOrThrowException() throws InvalidImportFileException {
		if (COLUMNNR_CNT_UNITS == DEFAULT_NOT_FOUND && COLUMNNR_STARTTIME == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_STARTTIME + " AND " + TEXT_CNT_UNITS + " not found.");
		} else if (COLUMNNR_CNT_UNITS == DEFAULT_NOT_FOUND && COLUMNNR_ENDTIME == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_ENDTIME + " AND " + TEXT_CNT_UNITS + " not found.");
		} else if (COLUMNNR_DATE == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_DATE + " not found.");
		} else if (COLUMNNR_DESCRIPTION == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_DESCRIPTION + " not found.");
		} else if (COLUMNNR_EMPLOYEE == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_EMPLOYEE + " not found.");
		} else if (COLUMNNR_PROJECT == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_PROJECT + " not found.");
		} else if (COLUMNNR_TASK == DEFAULT_NOT_FOUND) {
			throw new InvalidImportFileException("Column for " + TEXT_TASK + " not found.");
		}
	}

	@Override
	protected void checkCellMatchingTopic(short columnNumber, Cell cell) {

		if (matches(cell.getStringCellValue(), TEXT_CORRECTIONINDICATOR)) {
			COLUMNNR_CORRECTIONINDICATOR = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_STARTTIME)) {
			COLUMNNR_STARTTIME = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_ENDTIME)) {
			COLUMNNR_ENDTIME = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_DATE)) {
			COLUMNNR_DATE = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_SOLDSERVICE)) {
			COLUMNNR_SOLDSERVICE = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_DESCRIPTION)) {
			COLUMNNR_DESCRIPTION = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_EMPLOYEE)) {
			COLUMNNR_EMPLOYEE = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_PROJECT)) {
			COLUMNNR_PROJECT = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_TASK)) {
			COLUMNNR_TASK = columnNumber;
		} else if (matches(cell.getStringCellValue(), TEXT_CNT_UNITS)) {
			COLUMNNR_CNT_UNITS = columnNumber;
		}
	}

}
