package de.hud.sapwatcher.importer.excel;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import de.hud.sapwatcher.exception.InvalidImportFileException;

abstract public class AbstractExcelColumnnumberKeeper {
	public final static int DEFAULT_NOT_FOUND = -1;

	abstract protected void resetValues();

	abstract protected void checkAllValuesFilledOrThrowException() throws InvalidImportFileException;

	abstract protected void checkCellMatchingTopic(short columnNumber, Cell cell);

	public void createColumnsBasedOnHeader(Row row) throws InvalidImportFileException {
		resetValues();

		short minCol = row.getFirstCellNum();
		short maxCol = row.getLastCellNum();
		for (short col = minCol; col < maxCol; col++) {
			Cell cell = row.getCell(col);
			if (cell == null) {
				continue;
			}
			checkCellMatchingTopic(col, cell);
		}
		checkAllValuesFilledOrThrowException();

	}

	protected boolean matches(String title, List<String> texts) {
		for (String text : texts) {
			if (text.trim().equalsIgnoreCase(title.trim())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
