package de.hud.sapwatcher.data;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class OutputDataEntry {
	private String name = "<LEER>";

	private Float hours = new Float(0);

	private String soldService = null;

	private Float sumPrices = new Float(0);

	private Map<String, OutputDataEntry> subEntries = new TreeMap<>();

	private OutputDataEntry(String name, String soldService, Float sumPrices) {
		this.name = name;
		this.soldService = soldService;
		this.sumPrices = sumPrices;
	}

	public OutputDataEntry(String name) {
		this(name, null, null);
	}

	public String getSoldService() {
		if ((soldService == null) || soldService.isEmpty()) {
			return "<LEER>";
		}
		return soldService;
	}

	public Float getHours() {
		return hours;
	}

	public String getName() {
		if (name.trim().isEmpty()) {
			return "<LEER>";
		}
		return name;
	}

	public void setHours(Float hours) {
		if (hours < 0) {
			throw new IllegalArgumentException("Hours may not be negative!");
		}
		this.hours = hours;
	}

	public void setName(String name) {
		if (name != null) {
			this.name = name.trim();
		}
	}

	public void addSubEntry(OutputDataEntry newEntry) {
		if (subEntries.containsKey(newEntry.getName())) {
			OutputDataEntry oldEntry = subEntries.get(newEntry.getName());
			oldEntry.setHours(oldEntry.getHours() + newEntry.getHours());
		} else {
			subEntries.put(newEntry.getName(), newEntry);
		}

		hours += newEntry.getHours();
		if (newEntry.getSumPrices() != null) {
			sumPrices += newEntry.getSumPrices();
		}
	}

	public Map<String, OutputDataEntry> getSubEntries() {
		return subEntries;
	}

	public void setSoldService(String soldService) {
		this.soldService = soldService;
	}

	public Float getSumPrices() {
		return sumPrices;
	}

	public void setSumPrices(Float sumPrices) {
		this.sumPrices = sumPrices;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
