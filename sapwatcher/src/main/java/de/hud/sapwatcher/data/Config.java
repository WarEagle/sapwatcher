package de.hud.sapwatcher.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Config {

	private static Logger logger = LogManager.getLogger(Config.class.getName());

	@XmlTransient
	private String configFilename = "sapwatcher.xml";

	@XmlElementWrapper(name = "customGroupings")
	@XmlElementRef()
	private ArrayList<CustomGrouping> customGroupings = new ArrayList<>();

	@XmlElementWrapper(name = "hourlyRates")
	@XmlElementRef()
	private ArrayList<HourlyRate> hourlyRates = new ArrayList<>();

	public Config() {
	}

	Config(String configFilename) {
		this.configFilename = configFilename;
	}

	public void setCustomGroupings(ArrayList<CustomGrouping> customGroupings) {
		this.customGroupings = customGroupings;
	}

	public ArrayList<CustomGrouping> getActiveCustomGroupings() {
		ArrayList<CustomGrouping> activeCustomGroupings = new ArrayList<>();
		for (CustomGrouping customGrouping : customGroupings) {
			if (customGrouping.isActive()){
				activeCustomGroupings.add(customGrouping);
			}
		}
		return activeCustomGroupings;
	}

	public ArrayList<CustomGrouping> getCustomGroupings() {
		return customGroupings;
	}

	public void setHourlyRates(ArrayList<HourlyRate> hourlyRates) {
		this.hourlyRates = hourlyRates;
	}

	public ArrayList<HourlyRate> getHourlyRates() {
		return hourlyRates;
	}

	public Map<String, Float> getHourlyRatesAsMap() {
		Map<String, Float> rc = new TreeMap<>();

		for (HourlyRate hourlyRate : hourlyRates) {
			if (hourlyRate.getRate() == null) {
				hourlyRate.setRate(0f);
			}
			if ((hourlyRate.getSoldService() == null) || (hourlyRate.getSoldService().isEmpty())) {
				continue;
			}
			rc.put(hourlyRate.getSoldService(), hourlyRate.getRate());
		}

		return rc;
	}

	public void readFromFile() throws JAXBException {
		logger.entry();
		Config conf;

		File file = new File(configFilename);
		if (file.exists()) {
			logger.info("Reading from " + configFilename);

			JAXBContext jc = JAXBContext.newInstance(Config.class);
			Unmarshaller u = jc.createUnmarshaller();
			Object element = u.unmarshal(file);

			conf = (Config) element;
		} else {
			logger.info("No old config found.");
			conf = new Config(configFilename);
		}
		// assign to local vars
		customGroupings = conf.customGroupings;
		hourlyRates = conf.hourlyRates;
		
		logger.exit();
	}

	public void writeToFile() throws JAXBException, IOException {
		logger.entry();

		logger.info("Writing to " + configFilename);

		JAXBContext jc = JAXBContext.newInstance(Config.class);
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		OutputStream os = new FileOutputStream(new File(configFilename));
		marshaller.marshal(this, os);

		os.flush();
		os.close();

		logger.exit();
	}
}
