package de.hud.sapwatcher.data;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang3.builder.ToStringBuilder;

@XmlRootElement(name = "hourlyRate")
@XmlAccessorType(XmlAccessType.FIELD)
public class HourlyRate {

	public HourlyRate() {
		// required for reading xml-file
	}

	@XmlTransient
	private SimpleStringProperty soldService = new SimpleStringProperty();

	@XmlTransient
	private SimpleFloatProperty rate = new SimpleFloatProperty();

	public void setSoldService(String soldService) {
		this.soldService.setValue(soldService);
	}
	public void setRate(Float rate) {
		this.rate.setValue(rate);
	}

	public HourlyRate(String soldService, Float rate) {
		this.soldService = new SimpleStringProperty(soldService);
		this.rate = new SimpleFloatProperty(rate);
	}

	@XmlElement
	public Float getRate() {
		return rate.get();
	}

	@XmlElement
	public String getSoldService() {
		return soldService.get();
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
