package de.hud.sapwatcher.data;

import java.time.LocalDateTime;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ImportsetMetadata {
	private int numberEntries;

	private int numberEmployee;

	private int numberProjects;

	private float countHours;

	private LocalDateTime startTime;

	private LocalDateTime endTime;

	public void setNumberEntries(int numberEntries) {
		this.numberEntries = numberEntries;
	}
	
	public void setNumberEmployee(int numberEmployee) {
		this.numberEmployee = numberEmployee;
	}
	
	public void setNumberProjects(int numberProjects) {
		this.numberProjects = numberProjects;
	}
	
	public void setCountHours(float countHours) {
		this.countHours = countHours;
	}
	
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}
	
	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}
	
	public int getNumberEntries() {
		return numberEntries;
	}

	public int getNumberEmployee() {
		return numberEmployee;
	}

	public int getNumberProjects() {
		return numberProjects;
	}

	public float getCountHours() {
		return countHours;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
