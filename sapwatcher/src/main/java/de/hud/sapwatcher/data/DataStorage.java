package de.hud.sapwatcher.data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DataStorage {

	private static Logger logger = LogManager.getLogger(DataStorage.class.getName());

	private List<WorkingTimeEntry> workingTimeEntries;

	private List<String> listNamesProjects = new ArrayList<>();

	private List<String> listTasks = new ArrayList<>();

	private List<String> listNamesEmployee = new ArrayList<>();

	private void preanalyse() {
		logger.entry();

		listNamesProjects.clear();
		listNamesEmployee.clear();
		listTasks.clear();

		for (WorkingTimeEntry workingTimeEntry : workingTimeEntries) {
			if (!listNamesEmployee.contains(workingTimeEntry.getEmployee())) {
				listNamesEmployee.add(workingTimeEntry.getEmployee());
			}
			if (!listNamesProjects.contains(workingTimeEntry.getProject())) {
				listNamesProjects.add(workingTimeEntry.getProject());
			}
			if (!listTasks.contains(workingTimeEntry.getTask())) {
				listTasks.add(workingTimeEntry.getTask());
			}
		}

		logger.exit();
	}

	public void storeWorkOrderEntries(List<WorkingTimeEntry> workingTimeEntries) {
		logger.entry();

		this.workingTimeEntries = workingTimeEntries;
		preanalyse();

		logger.exit();
	}

	public List<WorkingTimeEntry> getWorkingTimeEntriesForEmployee(String employee) {
		logger.entry(employee);

		List<WorkingTimeEntry> rc = new ArrayList<>();

		for (WorkingTimeEntry workingTimeEntry : workingTimeEntries) {
			if (employee.equalsIgnoreCase(workingTimeEntry.getEmployee())) {
				rc.add(workingTimeEntry);
			}
		}

		return logger.exit(rc);
	}

	public List<WorkingTimeEntry> getWorkingTimeEntriesForProject(String project) {
		logger.entry(project);

		List<WorkingTimeEntry> rc = new ArrayList<>();

		for (WorkingTimeEntry workingTimeEntry : workingTimeEntries) {
			if (project.equalsIgnoreCase(workingTimeEntry.getProject())) {
				rc.add(workingTimeEntry);
			}
		}

		return logger.exit(rc);
	}

	public List<WorkingTimeEntry> getWorkingTimeEntriesInTimeperiod(LocalDate start, LocalDate end) {
		logger.entry(start, end);

		List<WorkingTimeEntry> rc = new ArrayList<>();

		if ((start == null) || (end == null)) {
			return logger.exit(rc);
		}

		for (WorkingTimeEntry workingTimeEntry : workingTimeEntries) {
			if ((start.isBefore(workingTimeEntry.getStartDate().toLocalDate()) || start.isEqual(workingTimeEntry.getStartDate().toLocalDate())) //
					&& end.isAfter(workingTimeEntry.getStartDate().toLocalDate()) || end.isEqual(workingTimeEntry.getStartDate().toLocalDate())) {
				rc.add(workingTimeEntry);
			}
		}

		return logger.exit(rc);
	}

	public Map<LocalDate, List<WorkingTimeEntry>> getWorkingTimeEntriesGroupedByDay() {
		Map<LocalDate, List<WorkingTimeEntry>> rc = new TreeMap<>();

		for (WorkingTimeEntry workingEntry : workingTimeEntries) {
			LocalDate date = workingEntry.getStartDate().toLocalDate();
			if (!rc.containsKey(date)) {
				rc.put(date, new ArrayList<>());
			}
			rc.get(date).add(workingEntry);
		}

		return rc;
	}

	public List<WorkingTimeEntry> getAllWorkingTimeEntries() {
		return workingTimeEntries;
	}

	public List<String> getListNamesEmployee() {
		return listNamesEmployee;
	}

	public List<String> getListNamesProjects() {
		return listNamesProjects;
	}
	
	public List<String> getListTasks() {
		return listTasks;
	}
}
