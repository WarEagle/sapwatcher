package de.hud.sapwatcher.data;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class OutputDataStructure {
	private String sectionName = "";

	private float budget = 0;

	private Float totalSumHours = new Float(0);

	private Float totalSumPrice = new Float(0);

	private Map<String, OutputDataEntry> entries = new TreeMap<>();

	public OutputDataStructure(String name, float budget) {
		this.sectionName = name;
		this.budget = budget;
	}

	public OutputDataStructure(String name) {
		this.sectionName = name;
	}

	public void addEntry(String name, Float hours) {
		addEntry(name, hours, null, new Float(0));
	}

	public void addEntry(String name, Float hours, String soldService, Float price) {
		OutputDataEntry entry;

		if (!entries.containsKey(name)) {
			entry = new OutputDataEntry(name);
			entries.put(name, entry);
		}
		entry = entries.get(name);

		entry.setHours(hours + entry.getHours());
		entry.setSoldService(soldService);
		if (price != null) {
			if (entry.getSumPrices() == null) {
				entry.setSumPrices(price);
			} else {
				entry.setSumPrices(price + entry.getSumPrices());
			}
			totalSumPrice += price;
		}

		totalSumHours += hours;
	}

	public void addSubEntry(String name, OutputDataEntry subEntry) {
		OutputDataEntry entry;

		if (!entries.containsKey(name)) {
			entries.put(name, new OutputDataEntry(name));
		}
		entry = entries.get(name);
		entry.addSubEntry(subEntry);

		totalSumHours += subEntry.getHours();
	}

	public Map<String, OutputDataEntry> getSubEntries() {
		return entries;
	}

	public String getSectionName() {
		if (sectionName.isEmpty()) {
			return "<LEER>";
		}
		return sectionName;
	}
	
	public float getBudget() {
		return budget;
	}

	public Float getTotalSumHours() {
		return totalSumHours;
	}

	public Float getTotalSumPrice() {
		return totalSumPrice;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
