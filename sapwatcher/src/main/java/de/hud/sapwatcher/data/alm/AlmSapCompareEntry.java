package de.hud.sapwatcher.data.alm;

public class AlmSapCompareEntry {

	private String plannedEmployee;

	private String requirementConcept;

	private String requirementId;

	private String requirementName;

	private String description;

	private String requirementStatus;

	private String requirementPriority;

	private Float effortPlanned;

	private String defectStatus;

	private String defectId;

	public String getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(String requirementId) {
		this.requirementId = requirementId;
	}

	public String getPlannedEmployee() {
		return plannedEmployee;
	}

	public void setPlannedEmployee(String plannedEmployee) {
		this.plannedEmployee = plannedEmployee;
	}

	public String getRequirementConcept() {
		return requirementConcept;
	}

	public void setRequirementConcept(String requirementConcept) {
		this.requirementConcept = requirementConcept;
	}

	public String getRequirementName() {
		return requirementName;
	}

	public void setRequirementName(String requirementName) {
		this.requirementName = requirementName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRequirementStatus() {
		return requirementStatus;
	}

	public void setRequirementStatus(String requirementStatus) {
		this.requirementStatus = requirementStatus;
	}

	public String getRequirementPriority() {
		return requirementPriority;
	}

	public void setRequirementPriority(String requirementPriority) {
		this.requirementPriority = requirementPriority;
	}

	public Float getEffortPlanned() {
		return effortPlanned;
	}

	public void setEffort(Float effortPlanned) {
		this.effortPlanned = effortPlanned;
	}

	public void setDefectId(String defectId) {
		this.defectId = defectId;
	}

	public void setDefectStatus(String defectStatus) {
		this.defectStatus = defectStatus;
	}

	public String getDefectId() {
		return defectId;
	}

	public String getDefectStatus() {
		return defectStatus;
	}

	@Override
	public String toString() {
		return "AlmSapCompareEntry [plannedEmployee=" + plannedEmployee + ", requirementConcept=" + requirementConcept + ", requirementId="
				+ requirementId + ", requirementName=" + requirementName + ", description=" + description + ", requirementStatus=" + requirementStatus
				+ ", requirementPriority=" + requirementPriority + ", effortPlanned=" + effortPlanned + ", defectStatus=" + defectStatus
				+ ", defectId=" + defectId + "]";
	}

}
