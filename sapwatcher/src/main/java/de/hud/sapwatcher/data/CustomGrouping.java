package de.hud.sapwatcher.data;

import java.util.regex.Pattern;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@XmlRootElement(name = "customGrouping")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomGrouping {

	public CustomGrouping() {
		// required for reading xml-file
	}

	private static Logger logger = LogManager.getLogger(CustomGrouping.class.getName());

	@XmlTransient
	private SimpleStringProperty name = new SimpleStringProperty();

	@XmlTransient
	private SimpleStringProperty filterProjectName = new SimpleStringProperty();

	@XmlTransient
	private SimpleStringProperty filterDescription = new SimpleStringProperty();

	@XmlTransient
	private SimpleStringProperty filterEmployee = new SimpleStringProperty();

	@XmlTransient
	private SimpleStringProperty filterTask = new SimpleStringProperty();

	@XmlTransient
	private SimpleBooleanProperty active = new SimpleBooleanProperty();

	@XmlTransient
	private SimpleFloatProperty budget = new SimpleFloatProperty();

	public void setFilterDescription(String filterDescription) {
		this.filterDescription.setValue(filterDescription);
	}

	public void setFilterProjectName(String filterProjectName) {
		this.filterProjectName.setValue(filterProjectName);
	}

	public void setFilterEmployee(String filterEmployee) {
		this.filterEmployee.setValue(filterEmployee);
	}

	public void setFilterTask(String filterTask) {
		this.filterTask.setValue(filterTask);
	}

	public void setName(String name) {
		this.name.setValue(name);
	}

	public void setBudget(Float budget) {
		this.budget.setValue(budget);
	}

	public void setActive(boolean active) {
		this.active.setValue(active);
	}

	public CustomGrouping(String name, boolean active, String filterProjectname, String filterDescription, String filterTask, String filterEmployee,
			Float budget) {
		this.name = new SimpleStringProperty(name);
		this.active = new SimpleBooleanProperty(active);
		this.filterProjectName = new SimpleStringProperty(filterProjectname);
		this.filterDescription = new SimpleStringProperty(filterDescription);
		this.filterEmployee = new SimpleStringProperty(filterEmployee);
		this.filterTask = new SimpleStringProperty(filterTask);
		this.budget = new SimpleFloatProperty(budget);
	}

	public CustomGrouping(String name, boolean active, String filterProjectname, String filterDescription, String filterTask, String filterEmployee) {
		this(name, active, filterProjectname, filterDescription, filterTask, filterEmployee, 0f);
	}

	@XmlElement
	public String getFilterDescription() {
		return filterDescription.get();
	}

	@XmlElement
	public String getFilterProjectName() {
		return filterProjectName.get();
	}

	@XmlElement
	public String getFilterTask() {
		return filterTask.get();
	}

	@XmlElement
	public String getFilterEmployee() {
		return filterEmployee.get();
	}

	@XmlElement
	public Float getBudget() {
		return budget.get();
	}

	@XmlElement
	public String getName() {
		return name.get();
	}

	@XmlElement
	public String getEmployee() {
		return filterEmployee.get();
	}

	public SimpleFloatProperty getBudgetProperty() {
		return budget;
	}

	public SimpleStringProperty getNameProperty() {
		return name;
	}

	public SimpleStringProperty getEmployeeProperty() {
		return filterEmployee;
	}

	public SimpleBooleanProperty activeProperty() {
		return active;
	}

	@XmlElement
	public boolean isActive() {
		return active.get();
	}

	public boolean isMatchingWorkingTimeEntry(WorkingTimeEntry entry) {
		logger.entry(entry);

		boolean rc = Boolean.FALSE;
		if (isActive() //
				&& matches(filterProjectName.get(), entry.getProject()) //
				&& matches(filterEmployee.get(), entry.getEmployee()) //
				&& matches(filterDescription.get(), entry.getDescription()) //
				&& matches(filterTask.get(), entry.getTask()) //
		) {
			rc = Boolean.TRUE;
		}

		return logger.exit(rc);
	}

	private boolean matches(String filter, String text) {
		boolean rc = Boolean.FALSE;

		if ((filter == null) || (filter.trim().length() == 0)) {
			rc = true;
		}

		if ((!rc) && (Pattern.matches(filter.toLowerCase(), text.toLowerCase()))) {
			rc = true;
		}

		return rc;

	}

	@Override
	public String toString() {
		StringBuffer rc = new StringBuffer();

		rc.append("[");

		rc.append("active=");
		rc.append(isActive());

		rc.append(", ");
		rc.append("name=");
		rc.append(getName());

		rc.append(", ");
		rc.append("filterProjectName=");
		rc.append(getFilterProjectName());

		rc.append(", ");
		rc.append("filterTask=");
		rc.append(getFilterTask());

		rc.append(", ");
		rc.append("filterDescription=");
		rc.append(getFilterDescription());

		rc.append(", ");
		rc.append("filterEmployee=");
		rc.append(getFilterEmployee());

		rc.append(", ");
		rc.append("budget=");
		rc.append(getBudget());

		rc.append("]");

		return rc.toString();
	}


}
