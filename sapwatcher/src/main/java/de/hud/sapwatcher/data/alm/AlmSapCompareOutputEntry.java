package de.hud.sapwatcher.data.alm;

import java.util.HashMap;
import java.util.Map;

public class AlmSapCompareOutputEntry {

	private String plannedEmployee;

	private String requirementConcept;

	private String requirementId;

	private String requirementName;

	private String description;

	private String requirementStatus;

	private String requirementPriority;

	private float effortPlanned;

	private float effortCurrentTotal;

	private Map<String, Float> effortCurrentDetails = new HashMap<>();

	public AlmSapCompareOutputEntry() {
		this.description = "---";
		this.requirementName = "---";
		this.plannedEmployee = "---";
		this.requirementStatus = "---";
		this.requirementPriority = "---";
		this.effortPlanned = 0f;
	}

	public AlmSapCompareOutputEntry(AlmSapCompareEntry entryAlm) {
		this.description = entryAlm.getDescription();
		this.plannedEmployee = entryAlm.getPlannedEmployee();
		this.requirementStatus = entryAlm.getRequirementStatus();
		this.requirementPriority = entryAlm.getRequirementPriority();
		this.effortPlanned = entryAlm.getEffortPlanned();
		this.requirementName = entryAlm.getRequirementName();
		this.requirementId = entryAlm.getRequirementId();
	}

	public String getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(String requirementId) {
		this.requirementId = requirementId;
	}

	public String getPlannedEmployee() {
		return plannedEmployee;
	}

	public void setPlannedEmployee(String plannedEmployee) {
		this.plannedEmployee = plannedEmployee;
	}

	public String getRequirementConcept() {
		return requirementConcept;
	}

	public void setRequirementConcept(String requirementConcept) {
		this.requirementConcept = requirementConcept;
	}

	public String getRequirementName() {
		return requirementName;
	}

	public void setRequirementName(String requirementName) {
		this.requirementName = requirementName;
	}

	public String getDescription() {
		return description == null ? "---" : description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRequirementStatus() {
		return requirementStatus;
	}

	public void setRequirementStatus(String requirementStatus) {
		this.requirementStatus = requirementStatus;
	}

	public String getRequirementPriority() {
		return requirementPriority;
	}

	public void setRequirementPriority(String requirementPriority) {
		this.requirementPriority = requirementPriority;
	}

	public float getEffortPlanned() {
		return effortPlanned;
	}

	public void setEffortPlanned(float effortPlanned) {
		this.effortPlanned = effortPlanned;
	}

	public float getEffortCurrentTotal() {
		return effortCurrentTotal;
	}

	public float getEffortDifference() {
		return effortCurrentTotal - effortPlanned;
	}

	public void setEffortCurrentDetails(Map<String, Float> effortCurrentDetails) {
		this.effortCurrentDetails = effortCurrentDetails;
		recalculateEffortSum();
	}
	
	public void recalculateEffortSum(){
		effortCurrentTotal = 0f;
		for (String key : effortCurrentDetails.keySet()) {
			effortCurrentTotal += effortCurrentDetails.get(key);
		}
	}
	
	public Map<String, Float> getEffortCurrentDetails() {
		return effortCurrentDetails;
	}

	@Override
	public String toString() {
		return "AlmSapCompareEntry [plannedEmployee=" + plannedEmployee + ", requirementConcept=" + requirementConcept + ", requirementName="
				+ requirementName + ", description=" + description + ", requirementStatus=" + requirementStatus + ", requirementPriority="
				+ requirementPriority + ", effortPlanned=" + effortPlanned + ", effortCurrentTotal=" + effortCurrentTotal + ", effortDifference="
				+ getEffortDifference() + ", effortCurrentDetails=" + effortCurrentDetails + "]";
	}

}
