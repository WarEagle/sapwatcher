package de.hud.sapwatcher.data;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.hud.sapwatcher.helper.IdFinder;

public class WorkingTimeEntry {

	private static final int MINUTES_PER_HOUR = 60;

	private static final int SECONDS_PER_MINUTE = 60;

	private static final int SECONDS_PER_HOUR = SECONDS_PER_MINUTE * MINUTES_PER_HOUR;

	private final static String STRING_ADDITIONAL_TASK = "Zusatzaufgaben";

	private String employee;

	private LocalDateTime startDate;

	private LocalDateTime endDate;

	private String project;

	private String task;

	private String description;

	private String soldService;

	public WorkingTimeEntry(String employee, LocalDateTime startDate, LocalDateTime endDate, String project,
							String task, String description, String soldService) {

		this.employee = employee;
		this.startDate = startDate;
		this.endDate = endDate;
		this.project = project;
		this.task = task;
		this.description = description;
		this.soldService = soldService;
	}

	public String getEmployee() {
		return employee;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public String getProject() {
		return project;
	}

	public String getTask() {
		return task;
	}

	public String getDescription() {
		return description;
	}

	public String getSoldService() {
		return soldService;
	}

	public float getDurationInHours() {
		Duration duration = Duration.between(getStartDate(), getEndDate());
		float rc = (float) duration.getSeconds() / (float) SECONDS_PER_HOUR;
		return rc;
	}

	/**
	 * Splits the description based on the IdFinder and returns a map with id->partDuration
	 */
	public Map<String, Float> getSplittedDescription() {
		Map<String, Float> rc = new TreeMap<>();

		List<String> ids = IdFinder.findIds(getDescription());

		for (String id : ids) {
			rc.put(id, getDurationInHours() / ids.size());
		}

		if (rc.keySet().size() == 0) {
			// if no ids were found, use the description
			rc.put(getDescription(), getDurationInHours());
		}

		return rc;
	}

	public boolean isAdditionalTask() {
		boolean isAdditionalTask = STRING_ADDITIONAL_TASK.equalsIgnoreCase(getTask());
		return isAdditionalTask;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append("WorkingTimeEntry");
		sb.append("[");
		sb.append("employee=");
		sb.append(employee);
		sb.append(", ");
		sb.append("startDate=");
		sb.append(startDate);
		sb.append(", ");
		sb.append("endDate=");
		sb.append(endDate);
		sb.append(", ");
		sb.append("duration=");
		sb.append(getDurationInHours());
		sb.append(", ");
		sb.append("project=");
		sb.append(project);
		sb.append(", ");
		sb.append("task=");
		sb.append(task);
		sb.append(", ");
		sb.append("additionalTask=");
		sb.append(isAdditionalTask());
		sb.append(", ");
		sb.append("description=");
		sb.append(description);
		sb.append(", ");
		sb.append("soldService=");
		sb.append(soldService);
		sb.append("]");

		return sb.toString();
	}

}
