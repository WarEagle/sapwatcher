package de.hud.sapwatcher.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IdFinder {

	private final static String REGEX_CONTAINS_DEFECT = "[Dd]([Ee]|[Ff]){0,2}([Ee][Cc][Tt]){0,1}#{0,1}\\s{0,1}[0-9]{4,5}";

	private final static String REGEX_CONTAINS_REQUIREMENT = "[Rr]([Ee]|[Qq]){0,2}([Uu][Ii][Rr][Ee][Mm][Ee][Nn][Tt]){0,1}#{0,1}\\s*[0-9]{4,5}";

	private final static String REGEX_CONTAINS_MATCHING_DIGITS = "[013-9]{1}[0-9]{3,4}";

	public final static String TEXT_REQUIREMENT = "Requirement";

	public final static String TEXT_DEFECT = "Defect";

	public static final String TEXT_OTHER  = "Sonstiges";

	/**
	 * If there is no "REQ" or "DEF" or anything else, then use the number as limiter, lower numbers are requirements,
	 * higher numbers are defects.
	 */
	private final int highestRequirementNumber = 6000;

	private static Logger logger = LogManager.getLogger(IdFinder.class.getName());

	public static List<String> findIds(String text) {
		List<String> rc = new ArrayList<>();

		StringBuffer workingText = new StringBuffer(text);

		IdFinder idFinder = new IdFinder();

		idFinder.findAll(rc, workingText, REGEX_CONTAINS_REQUIREMENT, TEXT_REQUIREMENT + " ");
		idFinder.findAll(rc, workingText, REGEX_CONTAINS_DEFECT, TEXT_DEFECT + " ");
		idFinder.findAll(rc, workingText, REGEX_CONTAINS_MATCHING_DIGITS);

		Collections.sort(rc);

		return rc;
	}

	/**
	 * Used to decide wether it is a defect or requirement if there is no descriptive text but only the number itself.
	 */
	private void findAll(List<String> rc, StringBuffer workingText, String regex) {
		List<String> tmpList = new ArrayList<>();
		findAll(tmpList, workingText, regex, "");
		for (String stringNr : tmpList) {
			int nr = Integer.parseInt(stringNr);

			if (nr >= highestRequirementNumber) {
				rc.add(TEXT_DEFECT + " " + stringNr);
			} else {
				rc.add(TEXT_REQUIREMENT + " " + stringNr);
			}
		}
	}

	private void findAll(List<String> rc, StringBuffer workingText, String regex, String prefix) {
		List<String> tmpList = new ArrayList<>();
		boolean foundSomething;
		do {
			foundSomething = false;

			logger.trace("workingText = '" + workingText + "'");
			List<String> foundEntries = find(workingText, regex, prefix);
			if (foundEntries.size() > 0) {
				logger.trace("matches " + prefix + "'" + regex + "'");
				tmpList.addAll(foundEntries);
				foundSomething = true;
			}

		} while (foundSomething);
		rc.addAll(tmpList);
	}

	private List<String> find(StringBuffer text, String regex, String prefix) {
		List<String> rc = new ArrayList<>();

		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(text);

		if (matcher.find()) {
			logger.trace("start=" + matcher.start());
			logger.trace("end=" + matcher.end());
			String tmp = text.substring(matcher.start(), matcher.end());
			text.delete(matcher.start(), matcher.end());
			logger.trace("found=" + tmp);
			tmp = removeAllNonNumbers(tmp);
			logger.trace("clean=" + tmp);

			rc.add(prefix + tmp);
		}

		return rc;
	}

	private String removeAllNonNumbers(String text) {
		String rc;

		rc = text.replaceAll("[^\\d]", "");

		return rc;
	}

}
