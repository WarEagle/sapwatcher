package de.hud.sapwatcher.helper;

import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.data.ImportsetMetadata;
import de.hud.sapwatcher.data.WorkingTimeEntry;

public class ImportsetMetadataAnalyser {
	
	private static Logger logger = LogManager.getLogger(ImportsetMetadataAnalyser.class.getName());

	public static ImportsetMetadata analyseData(DataStorage dataStorage) {
		logger.entry(dataStorage);
		
		ImportsetMetadata rc = new ImportsetMetadata();

		LocalDateTime starttime = LocalDateTime.now();
		LocalDateTime endtime = LocalDateTime.now().minusYears(100);
		float hours = 0;

		for (WorkingTimeEntry workingTimeEntry : dataStorage.getAllWorkingTimeEntries()) {

			hours += workingTimeEntry.getDurationInHours();

			if (workingTimeEntry.getEndDate().isAfter(endtime)) {
				endtime = workingTimeEntry.getEndDate();
			}
			if (workingTimeEntry.getStartDate().isBefore(starttime)) {
				starttime = workingTimeEntry.getStartDate();
			}
		}

		rc.setNumberEmployee(dataStorage.getListNamesEmployee().size());
		rc.setNumberEntries(dataStorage.getAllWorkingTimeEntries().size());
		rc.setNumberProjects(dataStorage.getListNamesProjects().size());
		rc.setCountHours(hours);
		rc.setStartTime(starttime);
		rc.setEndTime(endtime);

		return logger.exit(rc);
	}

}
