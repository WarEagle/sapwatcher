package de.hud.sapwatcher.helper;

import java.util.Map;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PriceCalculator {
	private static Logger logger = LogManager.getLogger(PriceCalculator.class.getName());

	private Map<String, Float> prices = new TreeMap<>();

	public void addPrice(String soldService, Float price) {
		prices.put(soldService, price);
	}

	public void setPrices(Map<String, Float> newPrices) {
		prices.clear();
		prices.putAll(newPrices);
	}

	public Map<String, Float> getPrices() {
		return prices;
	}

	public Float calculateSum(String soldService, Float hours) {
		logger.entry(soldService, hours);

		Float rc;

		if (hours < 0) {
			logger.error("Hours must note be negative.");
			return new Float(0);
		} else if ((soldService == null) || (soldService.isEmpty())) {
			return new Float(0);
		} else if (!prices.containsKey(soldService)) {
			logger.warn("Service '" + soldService + "' unknown.");
			return new Float(0);
		}
		Float price = prices.get(soldService);

		rc = hours * price;

		return logger.exit(rc);
	}
}
