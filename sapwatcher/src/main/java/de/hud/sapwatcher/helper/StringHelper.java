package de.hud.sapwatcher.helper;

import java.time.LocalDateTime;
import java.util.Date;

public class StringHelper {

	public static boolean isNotEmpty(String text) {
		return !isEmpty(text);
	}

	public static boolean isEmpty(String text) {
		boolean rc = false;
		if ((null == text)//
				|| (text.isEmpty())//
				|| (text.trim().isEmpty())//
				|| ("<LEER>".equalsIgnoreCase(text.trim()))//
				|| (".".equalsIgnoreCase(text.trim()))//
				|| ("..".equalsIgnoreCase(text.trim()))//
				|| ("...".equalsIgnoreCase(text.trim()))//
		) { //
			rc = true;
		}
		return rc;
	}

	public static String shortenName(String longName) {
		StringBuffer shortName = new StringBuffer();

		if (longName == null) {
			return shortName.toString();
		}

		String[] splitted = longName.split(" ");
		for (String string : splitted) {
			shortName.append(string.charAt(0));
			if (string.length() > 1) {
				shortName.append(string.charAt(1));
			}
			shortName.append(".");
		}

		return shortName.toString();
	}

	public static LocalDateTime createTimeFromPartialStrings(Date date, Date time) {

		if (date == null) {
			throw new IllegalArgumentException("date is null, time=" + time);
		} else if (time == null) {
			throw new IllegalArgumentException("time is null, date=" + date);
		}

		@SuppressWarnings("deprecation")
		LocalDateTime rc = LocalDateTime.of(date.getYear() + 1900, date.getMonth() + 1, date.getDate(), time.getHours(), time.getMinutes(),
				time.getSeconds());

		return rc;
	}

	public static String shortenTo(String text, int length) {
		if (text == null){
			return null;
		}
		return text.substring(0, Math.min(length, text.length()));
	}
}
