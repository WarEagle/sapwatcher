package de.hud.sapwatcher.processor.summary;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.OutputDataEntry;
import de.hud.sapwatcher.data.OutputDataStructure;
import de.hud.sapwatcher.data.WorkingTimeEntry;

public class EmployeePerProjectGenerator extends AbstractDataGenerator {
	private static Logger logger = LogManager.getLogger(EmployeePerProjectGenerator.class.getName());

	public EmployeePerProjectGenerator() {
		initOutputDataStructures();
	}

	public List<OutputDataStructure> fillDataStructure() {
		logger.entry();

		initOutputDataStructures();

		List<String> listNamesProjects = dataStorage.getListNamesProjects();

		for (String project : listNamesProjects) {
			List<WorkingTimeEntry> workingTimeEntriesForProject = dataStorage.getWorkingTimeEntriesForProject(project);

			processWorkingEntriesForProject(project, workingTimeEntriesForProject);
		}

		List<OutputDataStructure> rc = getNonEmptyOutputStructures();

		return logger.exit(rc);
	}

	private void processWorkingEntriesForProject(String projectName, List<WorkingTimeEntry> workingTimeEntriesForProject) {

		for (WorkingTimeEntry workingTimeEntry : workingTimeEntriesForProject) {
			OutputDataStructure outputDataStructure = createOrGetOutputDataStructure(projectName, workingTimeEntry.getTask());

			Map<String, Float> splittedDescription = workingTimeEntry.getSplittedDescription();
			for (String description : splittedDescription.keySet()) {
				processDescription(outputDataStructure, workingTimeEntry, splittedDescription, description);
			}

		}
	}

	private void processDescription(OutputDataStructure outputDataStructure, WorkingTimeEntry workingTimeEntry,
			Map<String, Float> splittedDescription, String description) {
		
		if (outputDescription.getValue()) {
			OutputDataEntry subEntry = new OutputDataEntry(description);
			subEntry.setHours(splittedDescription.get(description));
			outputDataStructure.addSubEntry(workingTimeEntry.getEmployee(), subEntry);
		} else {
			outputDataStructure.addEntry(workingTimeEntry.getEmployee(), splittedDescription.get(description));
		}

	}
}
