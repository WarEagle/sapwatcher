package de.hud.sapwatcher.processor.summary;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.OutputDataStructure;
import de.hud.sapwatcher.data.WorkingTimeEntry;
import de.hud.sapwatcher.helper.IdFinder;

public class SummaryGenerator extends AbstractDataGenerator {
	private static Logger logger = LogManager.getLogger(SummaryGenerator.class.getName());

	@Override
	public List<OutputDataStructure> fillDataStructure() {
		logger.entry();

		initOutputDataStructures();

		for (WorkingTimeEntry workingTimeEntry : dataStorage.getAllWorkingTimeEntries()) {
			processWorkingTimeEntry(workingTimeEntry);
		}

		List<OutputDataStructure> rc = getNonEmptyOutputStructures();
		
		return logger.exit(rc);
	}

	private void processWorkingTimeEntry(WorkingTimeEntry workingTimeEntry) {

		Map<String, Float> tmpTask2time = workingTimeEntry.getSplittedDescription();

		for (String task : tmpTask2time.keySet()) {
			addToCorrectOutputStructure(tmpTask2time, workingTimeEntry, task);
		}
	}

	private void addToCorrectOutputStructure(Map<String, Float> tmpTask2time, WorkingTimeEntry workingTimeEntry, String task) {

		OutputDataStructure outputDataStructure;
		if (task.startsWith(IdFinder.TEXT_REQUIREMENT)) {
			outputDataStructure = createOrGetOutputDataStructure(IdFinder.TEXT_REQUIREMENT, workingTimeEntry.getTask());
			outputDataStructure.addEntry(task, tmpTask2time.get(task));
		} else if (task.startsWith(IdFinder.TEXT_DEFECT)) {
			outputDataStructure = createOrGetOutputDataStructure(IdFinder.TEXT_DEFECT, workingTimeEntry.getTask());
			outputDataStructure.addEntry(task, tmpTask2time.get(task));
		} else {
			outputDataStructure = createOrGetOutputDataStructure(IdFinder.TEXT_OTHER, workingTimeEntry.getTask());
			outputDataStructure.addEntry(task, tmpTask2time.get(task));
		}
	}
}
