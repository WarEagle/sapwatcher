package de.hud.sapwatcher.processor.summary;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.OutputDataEntry;
import de.hud.sapwatcher.data.OutputDataStructure;
import de.hud.sapwatcher.data.WorkingTimeEntry;

public class TasksPerProjectGenerator extends AbstractDataGenerator {

	private static Logger logger = LogManager.getLogger(TasksPerProjectGenerator.class.getName());

	@Override
	public List<OutputDataStructure> fillDataStructure() {
		logger.entry();

		initOutputDataStructures();

		List<String> listNamesProjects = dataStorage.getListNamesProjects();
		for (String project : listNamesProjects) {
			OutputDataStructure outputDataStructure;

			for (WorkingTimeEntry workingTimeEntry : dataStorage.getWorkingTimeEntriesForProject(project)) {
				outputDataStructure = createOrGetOutputDataStructure(project, workingTimeEntry.getTask());
				Map<String, Float> splittedDescription = workingTimeEntry.getSplittedDescription();
				for (String description : splittedDescription.keySet()) {
					processDescription(outputDataStructure, workingTimeEntry, splittedDescription, description);
				}
			}
		}
		List<OutputDataStructure> rc = getNonEmptyOutputStructures();

		return logger.exit(rc);
	}

	private void processDescription(OutputDataStructure outputDataStructure, WorkingTimeEntry workingTimeEntry,
			Map<String, Float> splittedDescription, String description) {
		if (outputEmployee.getValue()) {
			OutputDataEntry subEntry = new OutputDataEntry(workingTimeEntry.getEmployee());
			subEntry.setHours(splittedDescription.get(description));
			outputDataStructure.addSubEntry(description, subEntry);
		} else {
			outputDataStructure.addEntry(description, splittedDescription.get(description));
		}
	}
}
