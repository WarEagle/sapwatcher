package de.hud.sapwatcher.processor.summary;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.CustomGrouping;
import de.hud.sapwatcher.data.OutputDataStructure;
import de.hud.sapwatcher.data.WorkingTimeEntry;
import de.hud.sapwatcher.helper.PriceCalculator;

public class CustomGroupingGenerator extends AbstractDataGenerator {
	private static Logger logger = LogManager.getLogger(CustomGroupingGenerator.class.getName());

	private PriceCalculator calculator;

	public CustomGroupingGenerator(PriceCalculator calculator) {
		this.calculator = calculator;
	}

	public List<OutputDataStructure> fillDataStructure() {
		logger.entry();

		List<OutputDataStructure> outputDataStructures = new ArrayList<>();
		List<CustomGrouping> customGroupings = config.getActiveCustomGroupings();

		for (CustomGrouping customGrouping : customGroupings) {
			List<WorkingTimeEntry> workingTimeEntries = dataStorage.getAllWorkingTimeEntries();
			OutputDataStructure outputDataStructure = new OutputDataStructure(customGrouping.getName(), customGrouping.getBudget());

			processCustomGrouping(customGrouping, workingTimeEntries, outputDataStructure);

			outputDataStructures.add(outputDataStructure);
		}

		return logger.exit(outputDataStructures);
	}

	private void processCustomGrouping(CustomGrouping customGrouping, List<WorkingTimeEntry> workingTimeEntries,
			OutputDataStructure outputDataStructure) {
		for (WorkingTimeEntry workingTimeEntry : workingTimeEntries) {
			if (customGrouping.isMatchingWorkingTimeEntry(workingTimeEntry)) {
				float price = calculator.calculateSum(workingTimeEntry.getSoldService(), workingTimeEntry.getDurationInHours());
				outputDataStructure.addEntry(workingTimeEntry.getEmployee(), workingTimeEntry.getDurationInHours(),
						workingTimeEntry.getSoldService(), price);
			}
		}
	}
}
