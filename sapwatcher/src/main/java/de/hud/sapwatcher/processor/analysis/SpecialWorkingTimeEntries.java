package de.hud.sapwatcher.processor.analysis;

import java.util.ArrayList;
import java.util.List;

import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.data.WorkingTimeEntry;
import de.hud.sapwatcher.helper.StringHelper;

public class SpecialWorkingTimeEntries {
	private DataStorage dataStorage;

	public SpecialWorkingTimeEntries(DataStorage dataStorage) {
		this.dataStorage = dataStorage;
	}

	public List<WorkingTimeEntry> findAllEmptyTasks() {
		List<WorkingTimeEntry> rc = new ArrayList<>();
		for (WorkingTimeEntry workingTimeEntry : dataStorage.getAllWorkingTimeEntries()) {
			if (isEmptyTask(workingTimeEntry)) {
				rc.add(workingTimeEntry);
			}
		}
		return rc;
	}

	public List<WorkingTimeEntry> findAllEmptyDescriptions() {
		List<WorkingTimeEntry> rc = new ArrayList<>();
		for (WorkingTimeEntry workingTimeEntry : dataStorage.getAllWorkingTimeEntries()) {
			if (isEmptyDescription(workingTimeEntry)) {
				rc.add(workingTimeEntry);
			}
		}
		return rc;
	}

	protected boolean isEmptyTask(WorkingTimeEntry workingTimeEntry) {
		boolean rc = StringHelper.isEmpty(workingTimeEntry.getTask());
		return rc;
	}

	protected boolean isEmptyDescription(WorkingTimeEntry workingTimeEntry) {
		boolean rc = StringHelper.isEmpty(workingTimeEntry.getDescription());
		return rc;
	}
}
