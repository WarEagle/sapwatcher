package de.hud.sapwatcher.processor.alm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.data.OutputDataEntry;
import de.hud.sapwatcher.data.OutputDataStructure;
import de.hud.sapwatcher.data.alm.AlmSapCompareEntry;
import de.hud.sapwatcher.data.alm.AlmSapCompareOutputEntry;
import de.hud.sapwatcher.helper.IdFinder;
import de.hud.sapwatcher.processor.summary.AllTasksGenerator;

public class AlmCompareGenerator {
	private static Logger logger = LogManager.getLogger(AlmCompareGenerator.class.getName());

	private AllTasksGenerator allTasksGenerator = new AllTasksGenerator();

	private List<AlmSapCompareEntry> entriesAlm = new ArrayList<>();

	private List<OutputDataEntry> processedEntries = new ArrayList<>();

	public void setAlmData(List<AlmSapCompareEntry> entriesAlm) {
		this.entriesAlm = entriesAlm;
	}

	public void setDataStorageSap(DataStorage dataStorage) {
		allTasksGenerator.setDataStorage(dataStorage);
	}

	public List<AlmSapCompareOutputEntry> compareEntries(boolean isAggregateDefectsToRequirements) {
		// init
		processedEntries = new ArrayList<>();
		allTasksGenerator.getOutputEmployeeProperty().set(true);
		List<OutputDataStructure> outputSap = allTasksGenerator.fillDataStructure();
		List<AlmSapCompareOutputEntry> outputAlm = new ArrayList<>();

		List<AlmSapCompareOutputEntry> rc = new ArrayList<>();

		// processing
		for (AlmSapCompareEntry entryAlm : entriesAlm) {
			addEverythingExistingInSap(entryAlm, outputSap, rc, isAggregateDefectsToRequirements);
		}
		outputAlm.addAll(rc);
		addEverythingMissingInSap(outputSap, outputAlm);

		if (isAggregateDefectsToRequirements) {
			aggregateDefectsToRequirements(outputAlm);
		}

		return outputAlm;
	}

	/**
	 * compress outputAlm to aggregate doublicates which are added due the
	 * aggregationsOfDefectsAndRequirements in those cases the requirement id is
	 * double once with all entries from the requirement and multiples from
	 * every defect assigned and booked on.
	 */
	private void aggregateDefectsToRequirements(List<AlmSapCompareOutputEntry> almData) {
		List<AlmSapCompareOutputEntry> aggregatedList = new ArrayList<>();

		for (AlmSapCompareOutputEntry existingEntry : almData) {
			boolean alreadyExists = false;
			for (AlmSapCompareOutputEntry aggregatedEntry : aggregatedList) {
				if (aggregatedEntry.getRequirementId().equalsIgnoreCase(existingEntry.getRequirementId())) {
					aggregateAlmEntry(aggregatedEntry, existingEntry);
					alreadyExists = true;
					break;
				}
			}
			if (!alreadyExists) {
				aggregatedList.add(existingEntry);
			}

		}

		almData.clear();
		almData.addAll(aggregatedList);

	}

	private void aggregateAlmEntry(AlmSapCompareOutputEntry aggregatedEntry, AlmSapCompareOutputEntry entryToAdd) {
		for (String key : entryToAdd.getEffortCurrentDetails().keySet()) {
			Map<String, Float> effortCurrentDetails = aggregatedEntry.getEffortCurrentDetails();
			if (effortCurrentDetails.containsKey(key)) {
				effortCurrentDetails.put(key, (effortCurrentDetails.get(key) + entryToAdd.getEffortCurrentDetails().get(key)));
			} else {
				effortCurrentDetails.put(key, entryToAdd.getEffortCurrentDetails().get(key));
			}
		}
		aggregatedEntry.recalculateEffortSum();

	}

	private void addEverythingMissingInSap(List<OutputDataStructure> outputSap, List<AlmSapCompareOutputEntry> rc) {
		// add entries which are in sap but not in alm
		for (OutputDataStructure outputDataStructure : outputSap) {
			for (String key : outputDataStructure.getSubEntries().keySet()) {
				OutputDataEntry subEntry = outputDataStructure.getSubEntries().get(key);
				if (processedEntries.contains(subEntry)) {
					continue;
				}
				// only look for requirements
				if (subEntry.getName().trim().startsWith(IdFinder.TEXT_REQUIREMENT)) {
					AlmSapCompareOutputEntry entry = new AlmSapCompareOutputEntry();
					entry.setRequirementId(subEntry.getName().replaceAll(IdFinder.TEXT_REQUIREMENT, "").trim());
					entry.setEffortCurrentDetails(convertSubentriesToDetailMap(subEntry.getSubEntries()));
					rc.add(entry);
				}
			}
		}
	}

	private void addEverythingExistingInSap(AlmSapCompareEntry entryAlm, List<OutputDataStructure> outputSap, List<AlmSapCompareOutputEntry> rc,
			boolean isAggregateDefectsToRequirements) {

		for (OutputDataStructure outputDataStructure : outputSap) {
			boolean wasInSap = addSapEntryIfExistsInSap(entryAlm, rc, outputDataStructure, isAggregateDefectsToRequirements);

			if (!wasInSap) {
				logger.debug("not found but exists=" + entryAlm);
				/*
				 * if it wasn't found in sap, then list it without used times,
				 * because it was planned
				 */
				AlmSapCompareOutputEntry entry = new AlmSapCompareOutputEntry(entryAlm);
				rc.add(entry);
			}
		} // END for
	}

	protected boolean addSapEntryIfExistsInSap(AlmSapCompareEntry entryAlm, List<AlmSapCompareOutputEntry> rc,
			OutputDataStructure outputDataStructure, boolean isAggregateDefectsToRequirements) {
		Map<String, OutputDataEntry> sapEntries = outputDataStructure.getSubEntries();

		boolean found = false;
		for (String entryName : sapEntries.keySet()) {
			OutputDataEntry sapEntry = sapEntries.get(entryName);

			if (sapEntry.getName().trim().endsWith(entryAlm.getRequirementId().trim())) {
				logger.debug("found=" + sapEntry);
				/*
				 * if the entry is already in the target collection, then we
				 * don't have to do anything. This is the processing of a
				 * requirement, it is already added, so ignore the rest.
				 */
				boolean requirementIdAlreadyProcessed = false;
				for (AlmSapCompareOutputEntry processedEntry : rc) {
					if (processedEntry.getRequirementId().endsWith(entryAlm.getRequirementId())) {
						requirementIdAlreadyProcessed = true;
						break;
					}
				}
				if (requirementIdAlreadyProcessed) {
					logger.debug("ignoring, because entry exists=" + sapEntry);
					found = true;
					break;
				}
				processedEntries.add(sapEntry);

				AlmSapCompareOutputEntry entry = new AlmSapCompareOutputEntry(entryAlm);
				entry.setEffortCurrentDetails(convertSubentriesToDetailMap(sapEntry.getSubEntries()));
				rc.add(entry);

				found = true;
			} else if (isAggregateDefectsToRequirements //
					&& (entryAlm.getDefectId() != null) //
					&& (entryAlm.getDefectId().trim().length() > 3)//
					&& sapEntry.getName().trim().endsWith(entryAlm.getDefectId().trim())) {
				logger.debug("found defect=" + sapEntry);

				AlmSapCompareOutputEntry entry = new AlmSapCompareOutputEntry(entryAlm);
				Map<String, Float> tmpMap = new HashMap<>();

				String employee = sapEntry.getSubEntries().values().iterator().next().getName();
				tmpMap.put("Defect " + entryAlm.getDefectId() + " " + employee, sapEntry.getHours());
				entry.setEffortCurrentDetails(tmpMap);
				entry.setDescription(entryAlm.getDescription());
				rc.add(entry);

				found = true;
			}
		}
		return found;
	}

	private Map<String, Float> convertSubentriesToDetailMap(Map<String, OutputDataEntry> subEntries) {
		Map<String, Float> rc = new HashMap<>();

		for (String key : subEntries.keySet()) {
			rc.put(key, subEntries.get(key).getHours());
		}

		return rc;
	}
}
