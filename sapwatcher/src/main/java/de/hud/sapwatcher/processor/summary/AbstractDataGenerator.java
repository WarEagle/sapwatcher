package de.hud.sapwatcher.processor.summary;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.hud.sapwatcher.data.Config;
import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.data.OutputDataStructure;
import de.hud.sapwatcher.helper.StringHelper;

public abstract class AbstractDataGenerator {
	private static Logger logger = LogManager.getLogger(SummaryGenerator.class.getName());

	private Map<String, OutputDataStructure> outputStructures;

	DataStorage dataStorage;

	Config config;

	BooleanProperty outputTasks = new SimpleBooleanProperty(false);

	BooleanProperty outputEmployee = new SimpleBooleanProperty(false);

	BooleanProperty outputDescription = new SimpleBooleanProperty(false);

	public BooleanProperty getOutputTasksProperty() {
		return outputTasks;
	}

	public BooleanProperty getOutputEmployeeProperty() {
		return outputEmployee;
	}

	public BooleanProperty getOutputDescriptionProperty() {
		return outputDescription;
	}

	abstract public List<OutputDataStructure> fillDataStructure();

	public void setConfig(Config config) {
		this.config = config;
	}

	public void setDataStorage(DataStorage dataStorage) {
		this.dataStorage = dataStorage;
	}

	void initOutputDataStructures() {
		logger.entry();

		outputStructures = new TreeMap<>();

		logger.exit();
	}

	OutputDataStructure createOrGetOutputDataStructure(String name, String task) {
		String title = name;
		if (outputTasks.getValue()) {
			if (StringHelper.isNotEmpty(task)) {
				title += " (" + task + ")";
			} else {
				title += " (<LEER>)";
			}
		}
		return createOrGetOutputDataStructure(title);
	}

	protected OutputDataStructure createOrGetOutputDataStructure(String name) {

		if (!outputStructures.containsKey(name)) {
			outputStructures.put(name, new OutputDataStructure(name));
		}
		return outputStructures.get(name);
	}

	List<OutputDataStructure> getNonEmptyOutputStructures() {
		List<OutputDataStructure> rc = new ArrayList<>();
		for (String name : outputStructures.keySet()) {
			OutputDataStructure outputDataStructure = outputStructures.get(name);
			if (outputDataStructure.getTotalSumHours() > 0) {
				rc.add(outputDataStructure);
			}
		}
		return rc;
	}

}
