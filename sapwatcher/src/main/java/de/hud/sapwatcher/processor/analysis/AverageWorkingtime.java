package de.hud.sapwatcher.processor.analysis;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.hud.sapwatcher.data.DataStorage;
import de.hud.sapwatcher.data.WorkingTimeEntry;

public class AverageWorkingtime {
	private DataStorage dataStorage;


	public AverageWorkingtime(DataStorage dataStorage) {
		this.dataStorage = dataStorage;
	}

	public Map<LocalDate, Float> aggregateByDayOfMonth(DataStorage dataStorage) {

		Map<LocalDate, List<WorkingTimeEntry>> tmpRc = dataStorage.getWorkingTimeEntriesGroupedByDay();

		// now aggregate to the hours
		Map<LocalDate, Float> rc = new HashMap<>();
		for (LocalDate date : tmpRc.keySet()) {
			List<WorkingTimeEntry> entries = tmpRc.get(date);
			Set<String> names = new HashSet<>();
			float hours = 0;
			for (WorkingTimeEntry workingTimeEntry : entries) {
				names.add(workingTimeEntry.getEmployee());
				hours += workingTimeEntry.getDurationInHours();
			}
			rc.put(date, hours / names.size());
		}
		return rc;

	}

	private Map<DayOfWeek, Float> aggregateByDayOfWeek(Map<LocalDate, Float> aggregatedByDay) {

		HashMap<DayOfWeek, Integer> entriesPerDay = new HashMap<>();
		HashMap<DayOfWeek, Float> hoursPerDay = new HashMap<>();

		for (LocalDate localDate : aggregatedByDay.keySet()) {
			DayOfWeek dayOfWeek = localDate.getDayOfWeek();
			if (!hoursPerDay.containsKey(dayOfWeek)) {
				hoursPerDay.put(dayOfWeek, 0f);
				entriesPerDay.put(dayOfWeek, 0);
			}
			hoursPerDay.put(dayOfWeek, hoursPerDay.get(dayOfWeek) + aggregatedByDay.get(localDate));
			entriesPerDay.put(dayOfWeek, entriesPerDay.get(dayOfWeek) + 1);
		}

		// and now add this to the output
		Map<DayOfWeek, Float> rc = new HashMap<>();

		for (DayOfWeek dayOfWeek : entriesPerDay.keySet()) {
			float hours = hoursPerDay.get(dayOfWeek) / entriesPerDay.get(dayOfWeek);
			rc.put(dayOfWeek, hours);
		}

		return rc;

	}

	public Map<DayOfWeek, Float> getTimePerWeekday() {
		Map<DayOfWeek, Float> rc;

		Map<LocalDate, Float> aggregatedByDay = aggregateByDayOfMonth(dataStorage);
		rc = aggregateByDayOfWeek(aggregatedByDay);

		return rc;
	}

}
